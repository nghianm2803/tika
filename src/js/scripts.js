//
// Scripts
//

window.addEventListener("DOMContentLoaded", (event) => {
  // Navbar shrink function
  var navbarShrink = function () {
    const navbarCollapsible = document.body.querySelector("#mainNav");
    if (!navbarCollapsible) {
      return;
    }
    if (window.scrollY === 0) {
      navbarCollapsible.classList.remove("navbar-shrink");
    } else {
      navbarCollapsible.classList.add("navbar-shrink");
    }
  };

  // Shrink the navbar
  navbarShrink();

  // Shrink the navbar when page is scrolled
  document.addEventListener("scroll", navbarShrink);

  // Activate Bootstrap scrollspy on the main nav elementTitle
  const mainNav = document.body.querySelector("#mainNav");
  if (mainNav) {
    new bootstrap.ScrollSpy(document.body, {
      target: "#mainNav",
      offset: 72,
    });
  }

  // Collapse responsive navbar when toggler is visible
  const navbarToggler = document.body.querySelector(".navbar-toggler");
  const responsiveNavItems = [].slice.call(document.querySelectorAll("#navbarResponsive .nav-link"));
  responsiveNavItems.map(function (responsiveNavItem) {
    responsiveNavItem.addEventListener("click", () => {
      if (window.getComputedStyle(navbarToggler).display !== "none") {
        navbarToggler.click();
      }
    });
  });
  const titleChange = ["Thống kê dữ liệu", "Phân tích giá trị", "Chuyên sâu lĩnh vực", "Dự đoán xu hướng"];
  const contentChange = [
    "Những số liệu chi tiết, chính xác và mới nhất sẽ luôn được cập nhật đến bạn",
    "Đánh giá có chiều sâu dựa trên thống kê nhằm đưa ra thông tin giá trị đằng sau các con số",
    "Những thông tin đặc thù được đưa ra dựa trên sự thấu hiểu và đặc thù lĩnh vực phân tích",
    "Đưa ra bức tranh toàn cảnh về sản phẩm, lĩnh vực và xu hướng trong thời gian gần dựa trên những đánh giá chuyên sâu",
  ];
  let count = 0;
  let count2 = 0;
  const elementTitle = document.getElementById("change-h1");
  const elementContent = document.getElementById("change-h2");
  const iteration = () => {
    elementTitle.innerHTML = titleChange[parseInt(count / 2, 10) % titleChange.length];
    elementContent.innerHTML = contentChange[parseInt(count2 / 2, 10) % contentChange.length];
    if (count % 2 !== 0) {
      elementTitle.classList.add("out");
      elementContent.classList.add("out");
    } else {
      elementTitle.classList.remove("out");
      elementContent.classList.remove("out");
    }
    count++;
    if (count === titleChange.length * 2) {
      count = 0;
    }
  };
  let inthandle = setInterval(iteration, 3000);
  iteration();
});

