//--- Dialog boxes
/**
 * Installation:
 * 1. Add <div id="dialog-window"></div>
 * 2. Run the same like in example
 * 
 * Example:
 * 
 	prepareDialogBox(1,"Are you sure?");
	$("#dialog-window").dialog({
		resizable: false,
		height: 180,
		modal: true,
		buttons: {
			'No': function(){
				$(this).dialog('close');
			},
			'Yes': function(){
				showDialogBox(3,null)
				$.post('/test/test',{ "id": id }, 
					function(data)
					{
						if (data.result == 1) //ok
						{
							closeDialogBox();
							$("#test_div_"+id).remove();
						}
						else //blad
						{
							showDialogBox(2,'Error! Please try one more time.')
						}
					},'json');
				
			},
		}
	});
 * 
 */

/**
 * Przygotowuje wnetrze okienka dialogowego
 * TODO: powinno tylko przygotowywac oikienko, a nie otwierac jak teraz
 * @param {Object} text - tekst w okienku
 * @param {Object} kind - rodzaj okienka, patrz cialo funkcji
 */
function prepareDialogBox(kind, text) {
  switch (kind) {
    case 1: //pytanie
      //iconClass = "ui-icon-alert";<span class="ui-icon '+iconClass+'" style="float:left; margin:0 7px 20px 0;"></span>
      title = lp._("Pytanie");
      if (text == null) text = lp._("Jesteś pewien?");
      $("#dialog-window").attr("title", title);
      $("#dialog-window").dialog({ title: title });
      $("#dialog-window").html('<p style="padding-top:10px">' + text + "</p>");
      break;
    case 2: //informacja
      //iconClass = "ui-icon-info";<span class="ui-icon '+iconClass+'" style="float:left; margin:0 7px 20px 0;"></span>
      title = lp._("Informacja");
      if (text == null) text = lp._("Informacja");
      $("#dialog-window").attr("title", title);
      $("#dialog-window").dialog({ title: title });
      $("#dialog-window").html('<p style="padding-top:10px">' + text + "</p>");
      break;
    case 3: //operacja w trakcie
      title = "";
      if (text == null)
        text = '<div align="center"><img src="/static/img/ajax-circle.gif" style="margin-top:30px" /></div>';
      else
        text =
          "<div>" +
          text +
          '</div><div align="center"><img src="/static/img/ajax-circle.gif" style="margin-top:30px" /></div>';
      $("#dialog-window").dialog({ dialogClass: "loaderModal" });
      $("#dialog-window").dialog({ title: title });
      $("#dialog-window").html("<p>" + text + "</p>");
      break;
  }
}

/**
 * Wyswietla okno dialogowe
 * @param {Object} kind  2 - informacyje z jednym guzikiem OK, 3 - operacja w trakcie bez zadnego guzika
 * @param {Object} text - jesli chcemy dać treść komunikatu
 */
function showDialogBox(kind, text) {
  prepareDialogBox(kind, text);
  switch (kind) {
    case 2: //informacja
      $("#dialog-window").dialog({
        modal: true,
        buttons: {
          OK: function () {
            $(this).dialog("close");
          },
        },
      });
      break;
    case 3: //operacja w trakcie
      $("#dialog-window").dialog({
        modal: true,
        buttons: {},
      });
      break;
  }
}

function closeDialogBox() {
  $("#dialog-window").dialog("close");
}
