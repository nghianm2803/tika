//zamyka div cookiePolicy z informacja o cookie i zapamietuje
function cookiePolicyClose() {
  var exdate = new Date();
  exdate.setDate(exdate.getDate() + 1095);
  document.cookie = "cookiePolicyClosed=1; expires=" + exdate.toUTCString() + "; path=/; domain=" + siteDomain;
  $(".cookiePolicy").remove();
}

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/; domain=" + panelDomain;
}

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") c = c.substring(1);
    if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
  }
  return "";
}

