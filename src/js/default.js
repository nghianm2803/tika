function validateContactForm(u) {
  var isValid = true;
  $("#err_name").text("");
  $("#err_email").text("");
  $("#ff").val("");

  if ($("#name").val().length <= 3) {
    $("#err_name").text(NAME_REQUIRED);
    isValid = false;
  }

  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  if ($("#email").val().length <= 5 || !emailReg.test($("#email").val())) {
    $("#err_email").text(WRONG_EMAIL);
    isValid = false;
  }

  if (isValid == true) {
    $("#ff").val("fcf");
    $("#contact_form").attr("action", u);
    $("#login_button").hide();
    $("#login_button_loader").show();
  }
  return isValid;
}

//jquery qtip defaualt settings
if ($.fn.qtip) {
  $.fn.qtip.defaults.style.classes = "ui-tooltip-light";
  $.fn.qtip.defaults.position.my = "bottom left";
  $.fn.qtip.defaults.position.at = "top left";
  $.fn.qtip.defaults.position.viewport = $(window);
  $.fn.qtip.defaults.position.adjust.x = 0;
  $.fn.qtip.defaults.position.adjust.y = 0;
}

function print_r(theObj) {
  if (theObj.constructor == Array || theObj.constructor == Object) {
    document.write("<ul>");
    for (var p in theObj) {
      if (theObj[p]) {
        if (theObj[p].constructor == Array || theObj[p].constructor == Object) {
          document.write("<li>[" + p + "] => " + typeof theObj + "</li>");
          document.write("<ul>");
          print_r(theObj[p]);
          document.write("</ul>");
        } else {
          document.write("<li>[" + p + "] => " + theObj[p] + "</li>");
        }
      }
    }
    document.write("</ul>");
  }
}

function clearField(id) {
  /*$('#'+id).hide('slow');*/
  $("#" + id).html("");
}

function calculateDaysBetweenTwoDates(date1, date2) {
  var d1 = new Date(date1);
  var d2 = new Date(date2);
  var diff = Math.abs((d2 - d1) / 86400000);
  return diff;
}

jQuery.fn.center = function () {
  this.css("position", "absolute");
  this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
  this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
  return this;
};

function parseUrlAfterHash() {
  var url = window.location.hash;
  var results_parse = [];

  if (url != "") {
    url = url.substr(1);

    var rows = url.split("&");

    $.each(rows, function (i, row) {
      var value = row.split("=");
      results_parse[value[0]] = value[1];
    });

    return results_parse;
  } else return false;
}

function parseUrlAfterHashWithUrlDcodeAndEncode() {
  var url = window.location.hash;
  var results_parse = [];

  if (url != "") {
    url = url.substr(1);

    var rows = url.split("&");

    $.each(rows, function (i, row) {
      var value = row.split("=");
      results_parse[value[0]] = urlEncode(urlDcode(xssProtect(value[1])));
    });

    return results_parse;
  } else return false;
}

function parseUrl() {
  var url = window.location.search;
  var results_parse = [];
  if (url != "") {
    url = url.substr(1);

    var rows = url.split("&");
    if (rows.length > 1) {
      $.each(rows, function (i, row) {
        var value = row.split("=");
        if (!isAllowedQueryParam(value[0])) {
          results_parse[value[0]] = xssProtect(value[1]);
        }
      });

      return Object.keys(results_parse).length > 1 ? results_parse : false;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

function getAllowedQueryParams() {
  var queryParams = "";
  if (window.location.search != "") {
    var url = window.location.search.substr(1);
    var paramsList = url.split("&");
    $.each(paramsList, function (i, param) {
      var value = param.split("=");
      if (isAllowedQueryParam(value[0])) {
        queryParams += "&" + value[0] + "=" + value[1];
      }
    });
  }
  return queryParams;
}

function isAllowedQueryParam(key) {
  return ["utm_source", "utm_medium", "utm_content", "utm_campaign", "pdfw"].includes(key);
}

//Funkcja zmienia tablice parametrow na czesc url'a uzywanego w searchResultsNew
function paramsToUrl() {
  var str = "";
  for (var key in params) {
    if (params.hasOwnProperty(key)) {
      if (key != "sid" && key != "p" && !isAllowedQueryParam(key)) {
        if (str != "") {
          str += "&";
        }
        str += key + "=" + params[key];
      }
    }
  }
  return str;
}

//Funkcja zmienia tablice parametrow na czesc url'a uzywanego w tylko przy zapisie mojego filtra - nie zapisujemy nie-filtrow
function paramsToUrlForSaveMyFilter() {
  var str = "";
  for (var key in params) {
    value = params[key];
    if (key != "sid" && key != "p" && key != "pc") {
      if (str != "") {
        str += "&";
      }
      str += key + "=" + value;
    }
  }
  return str;
}

function goto(url) {
  if (url.substr(0, 7) != "http://" && url.substr(0, 8) != "https://") url = "http://" + url;
  if (url.length > 3) document.location.href = url;
}

function gotoNewWindow(url) {
  if (url.substr(0, 7) != "http://" && url.substr(0, 8) != "https://") url = "http://" + url;
  if (url.length > 3) window.open(url);
}

function reload() {
  window.location.reload();
}

//zamyka div cookiePolicy z informacja o cookie i zapamietuje
function cookiePolicyClose() {
  var exdate = new Date();
  exdate.setDate(exdate.getDate() + 1095);
  document.cookie = "cookiePolicyClosed=1; expires=" + exdate.toUTCString() + "; path=/; domain=" + siteDomain;
  $(".cookiePolicy").remove();
}

//odpowiednik php urlencode()
function urlEncode(str) {
  str = (str + "").toString();
  return encodeURIComponent(str)
    .replace(/!/g, "%21")
    .replace(/'/g, "%27")
    .replace(/\(/g, "%28")
    .replace(/\)/g, "%29")
    .replace(/\*/g, "%2A")
    .replace(/#/g, "%23")
    .replace(/%20/g, "+")
    .replace(/%2C/g, ",");
}

//odpowiednik php urlencode()
function urlDcode(str) {
  return decodeURIComponent((str + "").replace(/\+/g, "%20"));
}

function clearDots(str) {
  return (str + "").toString().replace(/\./g, "");
}

//odpowiednik php addslashes()
function addslashes(string) {
  return string
    .replace(/\\/g, "\\\\")
    .replace(/\u0008/g, "\\b")
    .replace(/\t/g, "\\t")
    .replace(/\n/g, "\\n")
    .replace(/\f/g, "\\f")
    .replace(/\r/g, "\\r")
    .replace(/'/g, "\\'");
}

//odpowiednik php get_html_translation_table
function get_html_translation_table(table, quote_style) {
  var entities = {},
    hash_map = {},
    decimal;
  var constMappingTable = {},
    constMappingQuoteStyle = {};
  var useTable = {},
    useQuoteStyle = {};

  // Translate arguments
  constMappingTable[0] = "HTML_SPECIALCHARS";
  constMappingTable[1] = "HTML_ENTITIES";
  constMappingQuoteStyle[0] = "ENT_NOQUOTES";
  constMappingQuoteStyle[2] = "ENT_COMPAT";
  constMappingQuoteStyle[3] = "ENT_QUOTES";

  useTable = !isNaN(table) ? constMappingTable[table] : table ? table.toUpperCase() : "HTML_SPECIALCHARS";
  useQuoteStyle = !isNaN(quote_style)
    ? constMappingQuoteStyle[quote_style]
    : quote_style
    ? quote_style.toUpperCase()
    : "ENT_COMPAT";

  if (useTable !== "HTML_SPECIALCHARS" && useTable !== "HTML_ENTITIES") {
    throw new Error("Table: " + useTable + " not supported");
    // return false;
  }

  entities["38"] = "&amp;";
  if (useTable === "HTML_ENTITIES") {
    entities["160"] = "&nbsp;";
    entities["161"] = "&iexcl;";
    entities["162"] = "&cent;";
    entities["163"] = "&pound;";
    entities["164"] = "&curren;";
    entities["165"] = "&yen;";
    entities["166"] = "&brvbar;";
    entities["167"] = "&sect;";
    entities["168"] = "&uml;";
    entities["169"] = "&copy;";
    entities["170"] = "&ordf;";
    entities["171"] = "&laquo;";
    entities["172"] = "&not;";
    entities["173"] = "&shy;";
    entities["174"] = "&reg;";
    entities["175"] = "&macr;";
    entities["176"] = "&deg;";
    entities["177"] = "&plusmn;";
    entities["178"] = "&sup2;";
    entities["179"] = "&sup3;";
    entities["180"] = "&acute;";
    entities["181"] = "&micro;";
    entities["182"] = "&para;";
    entities["183"] = "&middot;";
    entities["184"] = "&cedil;";
    entities["185"] = "&sup1;";
    entities["186"] = "&ordm;";
    entities["187"] = "&raquo;";
    entities["188"] = "&frac14;";
    entities["189"] = "&frac12;";
    entities["190"] = "&frac34;";
    entities["191"] = "&iquest;";
    entities["192"] = "&Agrave;";
    entities["193"] = "&Aacute;";
    entities["194"] = "&Acirc;";
    entities["195"] = "&Atilde;";
    entities["196"] = "&Auml;";
    entities["197"] = "&Aring;";
    entities["198"] = "&AElig;";
    entities["199"] = "&Ccedil;";
    entities["200"] = "&Egrave;";
    entities["201"] = "&Eacute;";
    entities["202"] = "&Ecirc;";
    entities["203"] = "&Euml;";
    entities["204"] = "&Igrave;";
    entities["205"] = "&Iacute;";
    entities["206"] = "&Icirc;";
    entities["207"] = "&Iuml;";
    entities["208"] = "&ETH;";
    entities["209"] = "&Ntilde;";
    entities["210"] = "&Ograve;";
    entities["211"] = "&Oacute;";
    entities["212"] = "&Ocirc;";
    entities["213"] = "&Otilde;";
    entities["214"] = "&Ouml;";
    entities["215"] = "&times;";
    entities["216"] = "&Oslash;";
    entities["217"] = "&Ugrave;";
    entities["218"] = "&Uacute;";
    entities["219"] = "&Ucirc;";
    entities["220"] = "&Uuml;";
    entities["221"] = "&Yacute;";
    entities["222"] = "&THORN;";
    entities["223"] = "&szlig;";
    entities["224"] = "&agrave;";
    entities["225"] = "&aacute;";
    entities["226"] = "&acirc;";
    entities["227"] = "&atilde;";
    entities["228"] = "&auml;";
    entities["229"] = "&aring;";
    entities["230"] = "&aelig;";
    entities["231"] = "&ccedil;";
    entities["232"] = "&egrave;";
    entities["233"] = "&eacute;";
    entities["234"] = "&ecirc;";
    entities["235"] = "&euml;";
    entities["236"] = "&igrave;";
    entities["237"] = "&iacute;";
    entities["238"] = "&icirc;";
    entities["239"] = "&iuml;";
    entities["240"] = "&eth;";
    entities["241"] = "&ntilde;";
    entities["242"] = "&ograve;";
    entities["243"] = "&oacute;";
    entities["244"] = "&ocirc;";
    entities["245"] = "&otilde;";
    entities["246"] = "&ouml;";
    entities["247"] = "&divide;";
    entities["248"] = "&oslash;";
    entities["249"] = "&ugrave;";
    entities["250"] = "&uacute;";
    entities["251"] = "&ucirc;";
    entities["252"] = "&uuml;";
    entities["253"] = "&yacute;";
    entities["254"] = "&thorn;";
    entities["255"] = "&yuml;";
  }

  if (useQuoteStyle !== "ENT_NOQUOTES") {
    entities["34"] = "&quot;";
  }
  if (useQuoteStyle === "ENT_QUOTES") {
    entities["39"] = "&#39;";
  }
  entities["60"] = "&lt;";
  entities["62"] = "&gt;";

  // ascii decimals to real symbols
  for (decimal in entities) {
    if (entities.hasOwnProperty(decimal)) {
      hash_map[String.fromCharCode(decimal)] = entities[decimal];
    }
  }

  return hash_map;
}

//odpowiednik php htmlentities
function htmlentities(string, quote_style, charset, double_encode) {
  var hash_map = get_html_translation_table("HTML_ENTITIES", quote_style),
    symbol = "";
  string = string == null ? "" : string + "";

  if (!hash_map) {
    return false;
  }

  if (quote_style && quote_style === "ENT_QUOTES") {
    hash_map["'"] = "&#039;";
  }

  if (!!double_encode || double_encode == null) {
    for (symbol in hash_map) {
      if (hash_map.hasOwnProperty(symbol)) {
        string = string.split(symbol).join(hash_map[symbol]);
      }
    }
  } else {
    string = string.replace(/([\s\S]*?)(&(?:#\d+|#x[\da-f]+|[a-zA-Z][\da-z]*);|$)/g, function (ignore, text, entity) {
      for (symbol in hash_map) {
        if (hash_map.hasOwnProperty(symbol)) {
          text = text.split(symbol).join(hash_map[symbol]);
        }
      }

      return text + entity;
    });
  }

  return string;
}

//odpowiednik php html_entity_decode
function html_entity_decode(string, quote_style) {
  var hash_map = {},
    symbol = "",
    tmp_str = "",
    entity = "";
  tmp_str = string.toString();

  if (false === (hash_map = this.get_html_translation_table("HTML_ENTITIES", quote_style))) {
    return false;
  }

  delete hash_map["&"];
  hash_map["&"] = "&amp;";

  for (symbol in hash_map) {
    entity = hash_map[symbol];
    tmp_str = tmp_str.split(entity).join(symbol);
  }
  tmp_str = tmp_str.split("&#039;").join("'");

  return tmp_str;
}

function number_format(number, decimals, dec_point, thousands_sep) {
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = typeof thousands_sep === "undefined" ? "," : thousands_sep,
    dec = typeof dec_point === "undefined" ? "." : dec_point,
    toFixedFix = function (n, prec) {
      // Fix for IE parseFloat(0.55).toFixed(0) = 0;
      var k = Math.pow(10, prec);
      return Math.round(n * k) / k;
    },
    s = (prec ? toFixedFix(n, prec) : Math.round(n)).toString().split(".");
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || "").length < prec) {
    s[1] = s[1] || "";
    s[1] += new Array(prec - s[1].length + 1).join("0");
  }
  return s.join(dec);
}

function xssProtect(str) {
  var $el = $("<div>");
  $el.text(str);
  return $el.html();
}

function imgError(id) {
  $("#mention_avatar_" + id + " a").html(
    '<img class="mention_thumb" src="/static/img/icons/categories/fa-category-web.png" border="0" />'
  );
}

function imgMentionsAvatarIconError(id, category) {
  $("#mention_avatar_" + id + " a").html(
    '<img src="/static/img/icons/categories/category-' + category + '.png" border="0" />'
  );
}

function imgSourceFaviconError(id) {
  $("#source_sites_" + id + " .favicon").html('<img src="/static/img/icons/categories/category-8.png" border="0" />');
}

function imgErrorMostInteractiveEntriesFromSocialMedia(id, category) {
  $("#most_interactive_entries_from_social_media_avatar_" + id).html(
    '<img width="54" height="54" class="img-avatar" src="/static/img/icons/categories/category-' +
      category +
      '.png" border="0" />'
  );
}

function imgErrorEntriesFromMostPopularAuthors(id, category) {
  $("#entries_from_most_popular_authors_avatar_" + id).html(
    '<img width="54" height="54" class="img-avatar" src="/static/img/icons/categories/category-' +
      category +
      '.png" border="0" />'
  );
}

function imgErrorMostImportantAuthors(id) {
  $("#most_important_authors_avatar_" + id).html(
    '<img width="20" height="20" class="img-avatar"  src="/static/img/icons/avatar_default_blue_small.png" border="0" />'
  );
}

function imgErrorMostActiveAuthors(id) {
  $("#most_active_authors_avatar_" + id).html(
    '<img width="20" height="20" class="img-avatar"  src="/static/img/icons/avatar_default_blue_small.png" border="0" />'
  );
}

function imgErrorQuotes(id) {
  $("#quotes_avatar_" + id).html(
    '<img width="32" height="32" class="img-avatar"  src="/static/img/icons/avatar_default_blue_small.png" border="0" />'
  );
}

function imgErrorService(id) {
  $("#service_icon_" + id).html("");
}

function imgErrorMention(id) {
  $("#mention_img_a_" + id).html("");
}

function setErr(id, text) {
  $("#err_" + id).html(text);
  $("#err_" + id).slideDown("slow");
  $("#" + id).addClass("err");
}

function clearErr(fields) {
  for (var i = 0; i < fields.length; i++) {
    $("#err_" + fields[i]).html("");
    $("#" + fields[i]).removeClass("err");
    $("#err_" + fields[i]).hide();
  }
}

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/; domain=" + panelDomain;
}

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") c = c.substring(1);
    if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
  }
  return "";
}

function loginByFacebook(response, signedRequest) {
  $("#login_button").after('<img id="login-loader" src="/static/img/loader.gif">');
  $(".login_button").toggle();
  $(".error").html("");

  var remember_me = 0;
  if ($("#remember_me").is(":checked")) {
    remember_me = 1;
  }

  var loginParams = new Array();
  loginParams["login"] = response.email;
  loginParams["password"] = response.id;
  loginParams["remember_me"] = remember_me;
  //wartosc odpowiada socialmedia_site_id z configa dla fb
  loginParams["outer_user_from"] = 1;
  //id usera w fb
  loginParams["outer_user_id"] = response.id;
  //signed request
  loginParams["outer_user_signed_request"] = signedRequest;

  return loginFormSubmit(loginParams);
}

function registerAccount() {
  registerAccountUsing($("#email").val(),$("#phonenumber").val(), $("#password").val(), "default");
}

function registerAccountByFacebook(response, signedRequest) {
  registerAccountUsing(response.email, response.id, "facebook", {
    id: response.id,
    signedRequest: signedRequest,
    from: 1,
  });
}

function registerAccountByGoogle(googleUser, token) {
  registerAccountUsing(googleUser.getEmail(), token, "google", {
    id: googleUser.getId(),
    signedRequest: token,
    from: 2,
    firstName: googleUser.getGivenName(),
    surname: googleUser.getFamilyName(),
  });
}

function loginByGoogle(googleUser, token) {
  $("#login_button").after('<img id="login-loader" src="/static/img/loader.gif">');
  $(".login_button").toggle();
  $(".error").html("");

  var rememberMe = 0;
  if ($("#remember_me").is(":checked")) {
    rememberMe = 1;
  }

  var loginParams = new Array();
  loginParams["login"] = googleUser.getEmail();
  loginParams["password"] = googleUser.getId();
  loginParams["remember_me"] = rememberMe;
  loginParams["outer_user_from"] = 2;
  loginParams["outer_user_id"] = googleUser.getId();
  loginParams["outer_user_signed_request"] = token;

  return loginFormSubmit(loginParams);
}

function registerAccountUsing(email, phone_number, password, registrationType, outerUser) {
  $("#register_button").after('<img id="register-loader" src="/static/img/loader.gif">');
  $("#register_button").removeAttr("disabled");
  $(".register_button").toggle();
  $(".error").html("");
  $(".error").removeClass("wrong-data");

  var params = {};
  var isValid = 1;
  var login = $("#login").val();

  if (email == "") {
    isValid = 0;
    $("#err_email").html(lp._("Podaj adres email"));
    $("#err_email").addClass("wrong-data");
  }

  if (phone_number == "") {
    isValid = 0;
    $("#err_phonenumber").html(lp._("Nhập số điện thoại"));
    $("#err_phonenumber").addClass("wrong-data");
  }

  if (password == "") {
    isValid = 0;
    $("#err_password").html(lp._("Hasło jest zbyt krótkie"));
    $("#err_password").addClass("wrong-data");
  }

  if (isValid) {
    params.registration_type = registrationType;
    params.login = login;
    params.email = email;
    params.password = password;
    params.phone_number = $("#phone_number").val();
    params.partner_code = $("#partner_code").val();
    params.communication_marketing_consent = $("#communication_marketing_consent").is(":checked") ? 1 : 0;
    params.automatic_activation = $("#automatic_activation").is(":checked") ? 1 : 0;
    params.client_category = $("#client_category").is(":checked") ? 1 : 0;
    params.make_me_caretaker = $("#make_me_caretaker").is(":checked") ? 1 : 0;
    params.registration_source = $("#registration_source").val();
    params.name_check = $("#username-check-input").val();
    params.recaptcha_token = $("#recaptcha_token").val();

    if (outerUser) {
      params.outer_user_from = outerUser.from;
      params.outer_user_id = outerUser.id;
      params.outer_user_signed_request = outerUser.signedRequest;
      params.outer_user_first_name = outerUser.firstName ? outerUser.firstName : "";
      params.outer_user_surname = outerUser.surname ? outerUser.surname : "";
    } else {
      params.outer_user_from = 0;
      params.outer_user_id = "";
      params.outer_user_signed_request = "";
      params.outer_user_first_name = "";
      params.outer_user_surnname = "";
    }

    registerAccountSubmit(params);
  } else {
    $(".register_button").toggle();
    $("#register-loader").remove();
  }
}

function registerAccountSubmit(params) {
  $(".error").html("");
  $(".error").removeClass("wrong-data");

  $.post(
    "/user/registration-submit/",
    params,
    function (data) {
      if (data && data.result > 0) {
        //gdy rejestracji dokonał user
        if (data.result == 1) {
          //jeśli ustawiony link do przekierowania po rejestracji
          if ($("#backurl").val()) {
            window.location.href = $("#backurl").val();
          } else {
            //jesli rejestracja z tymczasowym haslem - przekieruj do strony do zmiany hasla
            if (data.temporary_password) {
              window.location.href = "/user/set-password/?from=registration";
            } else {
              window.location.href = "/panel/results/?from=registration&sid=0";
            }
          }
          return;
        }

        //gdy rejestracji dokonał administrator
        if (data.result == 2) {
          window.location.href = "/user/register-account/step/registered";
          return;
        }

        //jeśli nie udało sie zalogować
        if (data.result == 3) {
          window.location.href = "/user/login/";
          return;
        }
      } else {
        if (data.errors) {
          $.each(data.errors, function (key, value) {
            $("#err_" + key).html(value);
            $("#err_" + key).addClass("wrong-data");
          });
        } else {
          $("#err_login_form").html(lp._("Wystąpił błąd! Prosimy spróbować ponownie."));
          $("#err_login_form").addClass("wrong-data");
        }

        $(".register_button").toggle();
        $("#email").removeAttr("disabled");
        $("#register-loader").remove();

        return;
      }
    },
    "json"
  );
}
/**
 * Metoda podmienia co 4 sekundy wyraz marka w glownym tekscie na stronie glownej
 */
var switchHomepageBrandWordTimeout = null;
function switchHomepageBrandWord() {
  clearTimeout(switchHomepageBrandWordTimeout);
  switchHomepageBrandWordTimeout = null;
  switchHomepageBrandWordTimeout = setTimeout("switchHomepageBrandWord()", 4000);
  var currentWordElement = $(".brand-word-visible");
  if (currentWordElement.hasClass("first-time-use")) {
    currentWordElement.removeClass("first-time-use");
    return;
  }
  $(".brand-word").removeClass("brand-word-visible");
  if (currentWordElement.next().length != 0) {
    currentWordElement = currentWordElement.next();
  } else {
    currentWordElement = $(".brand-word-first");
  }
  var currentWordElementClone = currentWordElement.clone().appendTo(".main-title");
  currentWordElementClone.css("position", "absolute");
  currentWordElementClone.css("display", "inline-block");
  var currentWordElementWdth = "-" + currentWordElement.width() + "px";
  currentWordElementClone.css("margin-left", currentWordElementWdth);
  var currentWordElementHeight = "-" + currentWordElement.height() + "px";
  currentWordElementClone.css("margin-top", currentWordElementHeight);
  currentWordElementClone.appendTo(".main-title");
  currentWordElement.addClass("brand-word-visible");
  currentWordElement.css("visibility", "hidden");
  currentWordElementClone.animate({ "margin-top": "0px" }, 200, function () {
    currentWordElementClone.remove();
    currentWordElement.css("visibility", "visible");
  });
}

/**
 * Metoda zwraca date jako objekt Date
 * @param string dateString
 * @return Date
 */
function strToDate(dateString) {
  var dateDay = [];
  var dateTime = [0, 0, 0];
  //warunek jesli podano date bez czasu
  if (dateString.length > 10) {
    var dateParts = dateString.split(" ");
    dateDay = dateParts[0].split("-");
    dateTime = dateParts[1].split(":");
  } else {
    dateDay = dateString.split("-");
  }
  var date = new Date(dateDay[0], dateDay[1] - 1, dateDay[2], dateTime[0], dateTime[1], dateTime[2]);

  return date;
}

/**
 * Metoda zwraca date jako timestamp
 * @param string dateString
 * @return timestamp
 */
function strtotime(dateString) {
  var date = strToDate(dateString);
  var dateTimestamp = date.getTime();

  return dateTimestamp;
}

/**
 * Metoda zwraca liczbę dni danego miesiąca
 * @param int timestamp
 */
function daysInMonth(timestamp) {
  var newDate = new Date(timestamp);
  newDate = new Date(newDate.getYear(), newDate.getMonth() + 1, 0).getDate();
  return newDate;
}

/**
 * Metoda poprawia wysokosc 2 boxow - elementowi 2 nadaje wysokosc 1
 * @param element1
 * @param element2
 */
function fixHeightOfTwoElements(element1, element2) {
  element2.height(element1.height());
}

//wyskoczenie z ramki - pozwalamy by webvisor.com mogl otwierac b24 w ramce
var windowTopLocation = window.top.location;
var windowTopLocationString = String(windowTopLocation);
if (window.self != window.top && windowTopLocationString.indexOf("webvisor.com") == -1) {
  window.top.location = window.self.location;
}

