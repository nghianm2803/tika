/** Methods to handle forms */
var FormUtils = new (function () {
  /**This variable stores unique itemSequence to generate unique ID for new fields*/
  this.newItemSequence = 0;
  this.generateNewItemSequence = function () {
    this.newItemSequence++;
    return this.newItemSequence;
  };

  /**Get ID from id="blblb_ID" field*/
  this.getId = function (item) {
    attrName = "id";
    text = $(item).attr(attrName);
    return text.substr(text.lastIndexOf("_") + 1);
  };

  /**Set ID to id="blblb_ID" field*/
  this.setId = function (item, id) {
    attrName = "id";
    text = $(item).attr(attrName);
    item.attr(attrName, text.substr(0, text.lastIndexOf("_") + 1) + id);
  };

  /**Get ID from id="blblb_ID" field*/
  this.getFor = function (item) {
    attrName = "for";
    text = $(item).attr(attrName);
    return text.substr(text.lastIndexOf("_") + 1);
  };

  /**Set ID to id="blblb_ID" field*/
  this.setFor = function (item, id) {
    attrName = "for";
    text = $(item).attr(attrName);
    item.attr(attrName, text.substr(0, text.lastIndexOf("_") + 1) + id);
  };

  /**Get INDEX from name="blabbla[INDEX]" field*/
  this.getNameIndex = function (item) {
    text = $(item).attr("name");
    //alert(text);
    index = text.match(/\[(.*)\]/);
    if (index) return index[1];
    else return null;
  };

  /**Set INDEX to name="blabbla[INDEX]" field*/
  this.setNameIndex = function (item, id) {
    text = $(item).attr("name");
    text = text.replace(/\[(.*)\]/, "[" + id + "]");
    //alert(text);
    $(item).attr("name", text);
  };

  //clears error div messages
  this.clearFormErrors = function () {
    $("div.error_form_div").each(function (index) {
      $(this).html("");
    });
  };
})();

/**Useful methods */
var ViewUtils = new (function () {
  this.ltrim = function (str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
  };

  this.rtrim = function (str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
  };

  this.trim = function (str, chars) {
    return this.ltrim(this.rtrim(str, chars), chars);
  };

  this.showLoader = function (element, width) {
    if (width > 0) widthHtml = "width=" + width + "px";
    else widthHtml = "";
    element.html('<img src="/static/img/ajax-circle.gif" alt="' + lp._("Wysyłanie") + '" ' + widthHtml + "/>");
  };
})();

