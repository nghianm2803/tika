lp.add2list("liczba wpisów", "Number of mentions");
lp.add2list("Wyników", "Mentions");
lp.add2list("wpisów", "mentions");
lp.add2list("Podaj adres email", "Nhập email của bạn");
lp.add2list("Hasło jest zbyt krótkie", "Mật khẩu quá ngắn");
lp.add2list("Wystąpił błąd! Prosimy spróbować ponownie.", "An error has occurred! Try again.");
lp.add2list("Pytanie", "Question");
lp.add2list("Jesteś pewien?", "Are you sure?");
lp.add2list("Informacja", "Information");
lp.add2list("opinie", "mentions");
lp.add2list("Liczba Wyników", "Number of mentions");
lp.add2list("Pokaż dodatkowe filtry", "Show additional filters");
lp.add2list("Ukryj dodatkowe filtry", "Hide additional filters");
lp.add2list("Błędna forma daty, poprawna forma to: RRRR-MM-DD", "Incorrect date format, should be YYYY-MM-DD");
lp.add2list("Uzupełnij poprawnie zakres dat", "Fill up the date range correctly");
lp.add2list("pokaż wpisy z danej strony", "show entries from this site");
lp.add2list("B/d", "N/A");
lp.add2list("pokaż wpisy autora", "show author’s entries");
lp.add2list("Nie znaleziono cytatu", "The quote has not been found");
lp.add2list("Lokalizacja", "Location");
lp.add2list("Brak danych o", "No data about");
lp.add2list("tags.mentions_add_all_selected", "Tag all marked");
lp.add2list("tags.mentions_remove_all_selected", "Untag all marked");
lp.add2list("tags.mentions_all_tag", "Global assignment of tag to mentions");
lp.add2list("tags.no_tags_added", "No tag has been added");
lp.add2list("tags.create", "Create tag");
lp.add2list("tags.untag_error", "An error occured while untagging the mention");
lp.add2list("tags.tags", "Tags");
lp.add2list("tags.tag_error", "An error occurred while tagging the mention");
lp.add2list("Pozytywny", "Positive");
lp.add2list("Neutralny", "Neutral");
lp.add2list("Negatywny", "Negative");
lp.add2list("Błąd podczas oznaczania sentymentu wpisu", "An error occurred while setting sentiment of the mention");
lp.add2list("Odznacz", "Unmark");
lp.add2list("Oznacz", "Mark");
lp.add2list("Błąd podczas oznaczania wpisu", "An error occurred while marking the mention");
lp.add2list("tags.select_tag_first", "Choose the mentions you want to work on by marking “select” or “select all” ");
lp.add2list("Nie", "No");
lp.add2list("Tak", "Yes");
lp.add2list("Filtr został usunięty", "Filter has been deleted");
lp.add2list("Brak filtrów", "No filters");
lp.add2list("Brak uprawnień", "No privileges");
lp.add2list(
  "Filtr, który próbujesz usunąć jest wykorzystywany w Powiadomieniu. Aby go usunąć musisz usunąć najpierw Powiadomienie.",
  "The filter you are trying to delete is used in the Notification settings. In order to delete the filter, delete the Notification first."
);
lp.add2list("Błąd podczas usuwania", "Error while deleting.");
lp.add2list("Na pewno chcesz skasować filtr?", "Are you sure you want to delete this filter?");
lp.add2list("tags.removed", "Tag has been removed");
lp.add2list("tags.no_tags", "No tags");
lp.add2list("tags.remove_question", "Are you sure you want to delete this tag?");
lp.add2list("Odznacz wszystkie", "Unmark all");
lp.add2list("Zaznacz wszystkie", "Select all");
lp.add2list("panel.results.unselect_all", "Unselect all");
lp.add2list("panel.results.select_all", "Select all");
lp.add2list("Pokaż wykres", "Show graph");
lp.add2list("Ukryj wykres", "Hide graph");
lp.add2list("Pokaż sentyment", "Show sentiment ");
lp.add2list("Ukryj sentyment", "Hide sentiment");
lp.add2list("Pokaż interakcje", "Show interactions");
lp.add2list("Ukryj interakcje", "Hide interactions");
lp.add2list("idź do", "go to");
lp.add2list("b/d", "N/A");
lp.add2list("Pokaż więcej", "Show more");
lp.add2list("Wystąpił błąd podczas pobierania autorów", "Error occurred while loading authors");
lp.add2list("Spróbuj ponownie", "Try again");
lp.add2list("Przejdź do strony", "Go to site");
lp.add2list("Zablokuj źródło", "Block the source");
lp.add2list("Wystąpił błąd podczas pobierania stron", "Error occurred while loading websites");
lp.add2list("Proces jest jednorazowy i potrwa tylko chwilę", "No worries. It Only Happens Once");
lp.add2list("Trwa analiza najpopularniejszych wzmianek", "Data analysis in progress");
lp.add2list("neutralny", "Neutral");
lp.add2list("pozytywny", "Positive");
lp.add2list("negatywny", "Negative");
lp.add2list("Influence Score", "Influencer Score");
lp.add2list("Top 100 najbardziej interaktywnych wpisów", "Top 100 Most interactive mentions");
lp.add2list("Pierwsza", "First");
lp.add2list("Poprzednia", "Previous");
lp.add2list("Następna", "Next");
lp.add2list("Ostatnia", "Last");
lp.add2list("Tymczasowy brak wyników w Social Media.", "Temporary lack of mentions from Social Media");
lp.add2list(
  "Wystąpił błąd podczas pobierania najpopularniejszych wzmianek",
  "Error occurred while loading the most popular mentions"
);
lp.add2list("Trwa analiza wpisów od najpopularniejszych autorów", "Data analysis in progress");
lp.add2list("Wpływ", "Influence");
lp.add2list("Dotarcie", "Reach ");
lp.add2list("Top 100 wpisów od najpopularniejszych autorów", "Top 100 Mentions from the most popular authors");
lp.add2list(
  "Wystąpił błąd podczas pobierania wpisów od najpopularniejszych autorów",
  "Error occurred while loading mentions from the most popular authors"
);
lp.add2list("Wystąpił błąd podczas pobierania danych do statystyk", "Error occurred while loading statistics");
lp.add2list("Brak wyników", "No mentions");
lp.add2list(
  "Wystąpił błąd podczas pobierania wykresu popularnych źródeł wyników",
  "Error occurred while loading a graph of popular sources of mentions"
);
lp.add2list(
  "Procentowy udział  wybranego autora we wpływie na dyskusję  na temat marki w Social Media. Wynik wyliczany jest jako stosunek procentowy liczby wypowiedzi autora,liczby jego znajomych lub śledzących  oraz współczynnika widzialności treści typowych dla wybranych serwisów społecznościowych  do wszystkich wypowiedzi na temat marki w Social Media.",
  "What part of the whole discussion was generated by the selected social media user / profile / fanpage."
);
lp.add2list("Share of Voice", "Voice share");
lp.add2list(
  "Wewnętrzny wskaźnik Brand24 używany do porównania wpływu różnych profilów social media w zadanym okresie. Wpływ szacowany jest na podstawie liczby wpisów autora na temat marki w Social Media, liczby jego znajomych lub śledzących.",
  "Estimated number of views generated by the selected author's containing the monitored phrase(s). Influence is based on the number of mentions, the number of followers / subscribers / friends and the typical visibility percentage for the selected social networks (this percentage describes how many of your friends / followers on average actually see your posts). "
);
lp.add2list("Szacowany wpływ", "Influence");
lp.add2list(
  "Wystąpił błąd podczas pobierania wpływu autorów Social Media",
  "Error occurred while loading influence of social media authors"
);
lp.add2list("Wypowiedzi", "Mentions");
lp.add2list(
  "Wystąpił błąd podczas pobierania najaktywniejszych autorów Social Media",
  "Error occurred while loading the most active social media authors"
);
lp.add2list(
  "Wystąpił błąd podczas pobierania najaktywniejszych stron",
  "Error occurred while loading the most active websites"
);
lp.add2list("Wizyt", "Visits");
lp.add2list(
  "Wystąpił błąd podczas pobierania najbardziej wpływowych stron",
  "Error occurred while loading the most influential websites"
);
lp.add2list("Projekt", "Project");
lp.add2list("Usuń projekt z porównania", "Delete project from comparison");
lp.add2list("Wyniki", "Mentions");
lp.add2list("Razem", "Total");
lp.add2list("Pozytywnych", "Positive");
lp.add2list("Negatywnych", "Negative");
lp.add2list("Źródła", "Sources");
lp.add2list("Pozytywne", "Positive");
lp.add2list("Negatywne", "Negative");
lp.add2list("Sentyment", "Sentiment");
lp.add2list("Mężczyźni", "Male");
lp.add2list("Kobiety", "Female");
lp.add2list("Płeć", "Gender");
lp.add2list("Wystąpił błąd podczas pobierania wyników", "Error occurred while processing the task");
lp.add2list("Wczytywanie danych", "Loading data");
lp.add2list("Szukaj w wynikach", "Search in mentions");
lp.add2list("Szukaj", "Search");
lp.add2list("Kobieta", "Female");
lp.add2list("Mężczyzna", "Male");
lp.add2list("Śledzących", "Followers");
lp.add2list("więcej", "more");
lp.add2list("mniej", "less");
lp.add2list("Miesięczna liczba wizyt na stronie", "Monthly number of visits on site");
lp.add2list("Pobieram interakcje", "Downloading interactions");
lp.add2list("Powiadom na email", "Share by email");
lp.add2list("Blokuj stronę", "Mute site");
lp.add2list("Blokuj autora", "Mute author");
lp.add2list("Odwiedź profil autora", "Visit author’s profile");
lp.add2list("Edycja", "Edit");
lp.add2list("Eksport do PDF", "Export to PDF");
lp.add2list("Komentarz", "Comment");
lp.add2list("Ustaw sentyment", "Set sentiment");
lp.add2list("panel.engage_mention_button_text", "Visit");
lp.add2list("Historia", "History");
lp.add2list("tags.tag", "Tag");
lp.add2list("tags.add_new", "Add new tag");
lp.add2list("Dodaj", "Add");
lp.add2list("Usuń", "Delete");
lp.add2list("mention.more_actions", "More actions");
lp.add2list("Wyczyść filtrowanie", "Clear filters");
lp.add2list("idź", "go");
lp.add2list("Opcje zbiorowe:", "Global settings");
lp.add2list("Odwiedzony", "Visited");
lp.add2list("Zasięg Social Media", "Social Media Reach");
lp.add2list("Polubień", "Likes");
lp.add2list("Udostępnień", "Shares");
lp.add2list("Komentarzy", "Comments");
lp.add2list("Liczba Polubień", "Number of likes");
lp.add2list("Liczba Udostępnień", "Number of Shares");
lp.add2list("Liczba Komentarzy", "Number of Comments");
lp.add2list("Liczba Pozytywnych", "Number of Positives");
lp.add2list("Liczba Negatywnych", "Number of Negatives");
lp.add2list("Błąd", "Error");
lp.add2list(
  "Wybrana opcja jest w trakcie budowy i obecnie jest niedostępna. Przepraszamy za utrudnienia i prosimy o cierpliwość.",
  "The chosen feature is under construction and it is not available at the moment. We apologize for any inconvenience."
);
lp.add2list(
  "Plan Abonamentowy Twojego konta nie pozwala na skorzystanie z wybranej opcji.",
  "Your current account subscription plan does not allow to use this option."
);
lp.add2list("Zobacz porównanie planów abonamentowych", "Please see the comparison of the subscription plans");
lp.add2list("Usuwanie wpisów...", "Deleting mentions...");
lp.add2list("Czy na pewno chcesz usunąć wybrane", "Are you sure you want to delete the selected");
lp.add2list("wyniki", "Mentions");
lp.add2list(
  "Chcesz usunąć tylko wynik czy również zablokować domenę, aby serwis nie pobierał z niej więcej wyników?",
  "Do you want to delete this individual record or block the entire domain? Mentions will not be gathered from blocked domains."
);
lp.add2list("Nie usuwaj", "Cancel");
lp.add2list("Usuń wynik", "Delete");
lp.add2list("Usuń wynik i zablokuj domenę", "Delete and Block");
lp.add2list(
  "Chcesz usunąć tylko wynik czy również zablokować autora wpisu, aby serwis nie pobierał więcej od niego wiadomości?",
  "Do you want to delete this individual mention or block this author? Mentions will not be gathered from blocked authors."
);
lp.add2list("Usuń wynik i zablokuj autora", "Delete and block");
lp.add2list("Czy na pewno chcesz usunąć wynik?", "Are you sure you want to delete this mention?");
lp.add2list(
  "Czy chcesz zablokować domenę i usunąć wszystkie zgromadzone z niej wyniki?",
  "Do you want to block this domain and delete all mentions from it?"
);
lp.add2list(
  "Czy chcesz zablokować domenę <b>%s</b> i usunąć wszystkie zgromadzone z niej wyniki?",
  "Do you want to block domain <b>%s</b> and delete all mentions from it?"
);
lp.add2list(
  "Czy chcesz zablokować autora <b>%s</b> i usunać wszystkie zgromadzone od niego wpisy?",
  "Do you want to block author <b>%s</b> and delete all mentions?"
);
lp.add2list("Tak, zablokuj domenę i usuń wyniki", "Block and Delete");
lp.add2list(
  "Czy chcesz zablokować autora i usunać wszystkie zgromadzone od niego wpisy?",
  "Do you want to block this author and delete all mentions?"
);
lp.add2list("Tak, zablokuj autora i usuń wyniki", "Block and Delete");
lp.add2list("Błąd podczas blokowania", "Error occured while blocking");
lp.add2list("Nie można zablokować tego autora", "You cannot block this author");
lp.add2list("Czy  na pewno chcesz skasować projekt?", "Are you sure you want to delete this project?");
lp.add2list("Projekt został usunięty", "Project has been deleted");
lp.add2list("Aktualne logo", "Current logo");
lp.add2list("zobacz", "See");
lp.add2list("lub", "or");
lp.add2list("usuń", "Delete");
lp.add2list("Zapisywanie...", "Saving...");
lp.add2list("Logo usunięte.", "Logo deleted");
lp.add2list("Pobieranie danych...", "Loading data");
lp.add2list("Trwa generowanie raportu", "The report is being created");
lp.add2list(
  "Raporty PDF nie są dostępne w planie abonamentowym Twojego konta",
  "PDF reports are not available in your subscription plan"
);
lp.add2list(
  "Więcej informacji oraz przykładowy raport znajdziesz",
  "More information and example of the report can be found here"
);
lp.add2list("tutaj", "here");
lp.add2list("Zmień", "Change");
lp.add2list("plan abonamentowy", "Subscription plan");
lp.add2list("Wystąpił błąd", "An error occurred");
lp.add2list(
  "Przesyłam interesujący wpis znaleziony w Internecie przez Brand24",
  "I am sending to you interesting mention found in the Internet by Brand24"
);
lp.add2list("Anuluj", "Cancel");
lp.add2list("Wyślij", "Send");
lp.add2list("Powiadomienie zostało wysłane", "Notification has been sent");
lp.add2list("Nowy filtr", "New filter");
lp.add2list("podaj email", "Enter e-mail address");
lp.add2list("Alert został utworzony", "Alert has been created");
lp.add2list("Na pewno chcesz utworzyć nowy alert?", "Are you sure you want to create a new alert?");
lp.add2list("Wprowadź prawidłowy email", "Enter a valid e-mail address");
lp.add2list("tags.adding.no_name", "You have to enter the tag name");
lp.add2list("tags.adding.no_privileges", "You have no privileges to create tags");
lp.add2list("Edytuj", "Edit");
lp.add2list("tags.add.success", "Tag has been added");
lp.add2list("Nazwa została zmieniona", "Name has been changed.");
lp.add2list(
  "Błąd podczas anulowania subskrybcji. Spróbuj jeszcze raz.",
  "An error occurred during the cancel your subscription process. Try once again."
);
lp.add2list("Słowo kluczowe musi mieć minimum dwa znaki", "Keyword must have at least two characters");
lp.add2list("Ups, nie podałeś/aś swoich słów kluczowych!", "Oops, you forgot to enter your keywords!");
lp.add2list("Zapisywanie konfiguracji projektu...", "Saving project configuration...");
lp.add2list("Popraw błąd formularza", "Form error - please correct it");
lp.add2list(
  "Wystąpił błąd podczas przetwarzania formularza. Odśwież stronę i spróbuj jeszcze raz",
  "An error has occurred. refresh the page and try again"
);
lp.add2list(
  "We wszystkich projektach możesz monitorować maksymalnie %s słów kluczowych. Obecnie wykorzystujesz już pełen limit. Jeśli chcesz dodać kolejne słowa, możesz: a) wykupić wyższy plan taryfowy konta z większą liczbą limitów b) dokupić podwyższenie limitu słów kluczowych c) usunąć inne słowa kluczowe, aby zwolnić limit",
  "In all your projects you can monitor maximum of %s  keywords. Currently you are using all available keywords for your account. If you want to add more keywords, you can: a) sign up for the higher subscription plan b) buy additional keywords for your account c) delete keywords that are already in use"
);
lp.add2list(
  "Nie można dodać kolejnego słowa kluczowego. Projekt zawiera już tak dużą ilość słów kluczowych,że skuteczność systemu monitoringu może spaść z powodu nadmiaru danych. Zalecamy podzielić zestaw słów kluczowych na dwie grupy i utworzyć dwa projekty.",
  "Cannot add new keywords. Project has too many keywords already and it can affect the accuracy of monitoring. We recommend to split the set of keywords into two thematic groups and create two separate projects."
);
lp.add2list(
  "Dodanie kolejnego słowa jest możliwe, ale niezalecane ze względu na skuteczność i szybkość działania monitoringu. Zamiast gromadzić wszystkie słowa kluczowe w jednym projekcie, zalecamy podzielenie ich na dwie grupy i utworzenie dwóch projektów.",
  "Adding new keyword is possible, but we do not recommend it. It may influence the speed and accuracy of monitoring. We recommend to divide this project into two separate projects."
);
lp.add2list("Wysyłanie", "Sending");
lp.add2list("Własny", "Own");
lp.add2list("Dzisiaj", "Today");
lp.add2list("Wczoraj", "Yesterday");
lp.add2list("Ostatnie 7 dni", "Last 7 days");
lp.add2list("Ostatnie 30 dni", "Last 30 days");
lp.add2list("Ostatnie 3 miesiące", "Last 3 months");
lp.add2list("Ostatni rok", "Last year");
lp.add2list("Wszystko", "All");
lp.add2list("Zobacz porównanie planów abonamentowych", "Please see the comparison of the subscription plans");
lp.add2list("Okres", "Period");
lp.add2list("Ustaw okres jako domyślny", "Set as default period");
lp.add2list("Anuluj", "Cancel");
lp.add2list("Zastosuj", "Apply");
lp.add2list("Dni", "Days");
lp.add2list("Tygodnie", "Weeks");
lp.add2list("Miesiące", "Months");
lp.add2list("Wybierz daty i okres", "Pick a date range and period");
lp.add2list("Obecnie wybrany projekt", "Your current project");
lp.add2list("Najnowsze Wyniki", "Latest Mentions");
lp.add2list("Wpływ", "Influence");
lp.add2list("Wypowiedzi", "Mentions");
lp.add2list("Wizyt", "Visits");
lp.add2list("Najbardziej wpływowe profile", "The most influential profiles");
lp.add2list("Tymczasowy brak wyników w Social Media.", "Temporary lack of mentions from Social Media");
lp.add2list("Najbardziej wpływowe strony", "The most influential sites");
lp.add2list("Zmień plan", "Upgrade");
lp.add2list("Najpopularniejsze wzmianki", "The most popular mentions");
lp.add2list("Niedostępne w Twoim planie.", "Not available for your plan.");
lp.add2list("Najnowsze wzmianki", "Latest mentions");
lp.add2list("Więcej wyników", "More mentions");
lp.add2list("Źródła", "Sources");
lp.add2list("Wyników poza social media", "Mentions beyond social media ");
lp.add2list("Wyników w social media", "Social media mentions");
lp.add2list("Komentarzy w social media", "Social media comments");
lp.add2list("Udostępnień w social media", "Social media shares");
lp.add2list("Polubień w social media", "Social media likes");
lp.add2list("Statystyki", "Stats");
lp.add2list("Obecny okres", "Current period");
lp.add2list("Poprzedni okres", "Previous period");
lp.add2list("Zasięg Social Media", "Social Media Reach");
lp.add2list("Podsumowanie", "Summary");
lp.add2list(
  "results.loading_box.data_download_progress_info",
  "Data download in progress. We specialize in collecting public data from the moment you setup a project onward."
);
lp.add2list("Pojawiły się nowe wzmianki w Twoim projekcie.", "We collected additional data for your project.");
lp.add2list("Kliknij, aby zobaczyć.", "Refresh to access.");
lp.add2list(
  "Jeśli Twoja spółka jest zarejestrowana w innym kraju skontaktuj się z ",
  "If company is registered in other countr, please contact"
);
lp.add2list("nami", "us");
lp.add2list("Zamknij", "Close");
lp.add2list("Nazwa firmy lub organizacji", "Company or organization name");
lp.add2list("Ulica i numer siedziby", "Address (street and number)");
lp.add2list("Imię i nazwisko", "Name and last name");
lp.add2list("Ulica i numer domu/mieszkania", "Address");
lp.add2list("Czy na pewno chcesz usunąć konto?", "Are you sure you want to delete this account?");
lp.add2list(
  "Czy na pewno chcesz anulować proces zmiany planu abonamentowego konta?",
  "Are you sure you want to cancel the process of subscription plan change?"
);
lp.add2list("Wystąpił błąd podczas przetwarzania żądania", "Error occurred while processing the task");
lp.add2list("Nie, chcę pozostać przy obecnym", "No, I want to stay with the present");
lp.add2list("Tak, chcę zmienić tryb płatności", "Yes, I want to change the form of payment");
lp.add2list(
  "Zgłoszenie przyjętę! Nasz konsultant skontaktuje się z Toba w przeciągu 24h",
  "Notification saved. Our consultant will contact you within 24 hours"
);
lp.add2list(
  "Na pewno chcesz zrezygnować z dostępu do konta?",
  "Are you sure you want to cancel your acces to the account?"
);
lp.add2list(
  "Czy na pewno chcesz dokonać zmiany planu abonamentowego konta?",
  "Are you sure you want to change the subscription plan for your account?"
);
lp.add2list(
  "Twoje konto wykorzystuje już pełen limit dostępów dla użytkowników. Możesz dokonać %s upgrade %s planu lub %s dokupić %s dodatkowy limit",
  "Your account uses the maximum number of user logins. You can upgrade %s %s your plan or %s buy %s extra logins"
);
lp.add2list(
  "Twój plan abonamentowy osiągnął limit dodatkowych użytkowników %s Zobacz cennik %s",
  "Your subscription plan reached extra users limit %s Check the pricing %s"
);
lp.add2list("Na pewno chcesz odebrać dostęp użytkownikowi?", "Are you sure you want to cancel access for this user?");
lp.add2list("Zapisz", "Save");
lp.add2list("Proszę czekać...", "Please wait...");
lp.add2list(
  "Dane zostały zapisane. Zostaniesz przekierowany do konfiguracji Slack...",
  "Your Slack integration settings have been saved. You'll now be redirected to Slack to complete your integration..."
);
lp.add2list("Musisz wybrać projekt", "You must select a project to continue");
lp.add2list("Edycja integracji Slack", "Slack Integration settings");
lp.add2list("Na pewno chcesz skasować integrację?", "Are you sure you want to disconnect your project from Slack?");
lp.add2list("Zaznacz co najmniej jeden typ powiadomienia", "Select one or more notification types");
lp.add2list("Wybierz częstotliwość", "Choose Frequency");
lp.add2list("o", "at");
lp.add2list("Filtr", "Filter");
lp.add2list("Wysyłane powyżej", "Send when more than");
lp.add2list("wyników", "Mentions");
lp.add2list("Brak", "None");
lp.add2list("Edycja powiadomienia", "Edit the notification");
lp.add2list("Dodawanie filtra", "Adding the new filter");
lp.add2list("Musisz podać nazwę filtra", "You have to enter the filter name");
lp.add2list("Musisz ustawić co najmniej jeden filtr", "You have to set at least one filter");
lp.add2list("Błąd połączenia. Proszę spróbować jeszcze raz", "Connection error. Try once again.");
lp.add2list("Nazwa firmy", "Company name");
lp.add2list("Ulica i numer", "Street and number");
lp.add2list("Pole nie może być puste", "Field can't be empty");
lp.add2list("Podaj poprawny numer", "Provide correct number");
lp.add2list("Trwa weryfikacja twoich danych...", "Verifying data...");
lp.add2list("Dane zostały zapisane", "Data have been saved");
lp.add2list("Popraw błędne dane do faktury", "Correct wrong invoice data");
lp.add2list("Trwa zapisywanie twoich danych...", "Saving data...");
lp.add2list("Trwa weryfikacja karty...", "Verifying card...");
lp.add2list("Problem podczas autoryzacji karty:", "Error in card authorization:");
lp.add2list("Trwa realizacja płatności...", "Processing payment...");
lp.add2list("Trwa aktualizacja danych konta...", "Updating account data...");
lp.add2list("Płatność została wykonana. Trwa aktualizacja danych konta...", "Payment made. Updating account data...");
lp.add2list("Brak zdefiniowanej akcji", "No defined action");
lp.add2list(
  "Czy chcesz anulować płatność i zrezygnować ze zmiany planu abonamentowego?",
  "Do you want to cancel the payment and resign from upgrading service plan?"
);
lp.add2list(
  "Czy chcesz anulować płatność i tym samym zrezygnować ze zmiany dodatkowych limitów?",
  "Do you want to cancel the payment and resign from changing additional limits?"
);
lp.add2list("Tak, chcę anulować", "Yes, i want to cancel");
lp.add2list(
  "Czy chcesz wyłączyć automatyczne opłacanie konta Twoją kartą kredytową?",
  "Do you want to turn on the automatic renewal payment with your credit card?"
);
lp.add2list(
  "Czy chcesz wyłączyć automatyczne opłacanie konta poprzez Paypal?",
  "Do you want to turn off automatic renewal by Paypal?"
);
lp.add2list(
  "Czy chcesz zapłacić używaną wcześniej kartą kredytową?",
  "Do you want to made the payment with a credit card, that you have used previously?"
);
lp.add2list("Numer karty:", "Credit Card Number:");
lp.add2list(
  "Czy chcesz zapłacić używanym wcześniej kontem Paypal?",
  "Do you want to made the payment with Paypal account, that you have used previously?"
);
lp.add2list("Email konta Paypal:", "Paypal email:");
lp.add2list("Nie, chcę wybrać inną metodę płatności", "No, I want to change the payment method");
lp.add2list("Tak, chcę zapłacić", "Yes, i want to pay");
lp.add2list("Operacja zakończona sukcesem", "Operation ended in success ");
lp.add2list("Zapisano", "Saved");
lp.add2list("Zmieniono kraj - potwierdź swój", "Country has been changed – confirm yours");
lp.add2list("Problem z ustawieniem kraju", "Problem with setting country ");
lp.add2list("Zmieniono formę prawną - potwierdź swój", "Legal form has been changed – confirm yours");
lp.add2list("Problem z ustawieniem formy prawnej", "Problem with setting legal form");
lp.add2list("Operacja zakończona sukcesem", "Operation ended in success ");
lp.add2list("Wystąpił błąd", "An error occurred");
lp.add2list(
  "Jeśli limit nie jest już przekroczony, to zbieranie zostanie odblokowane. Jeśli limit jest przekroczony, a chcesz odblokować, to musisz najpierw zmienić limit",
  "If the limit is not already exceeded, then collection will be blocked. If the limit is exceeded and you want to unblock it, then you have to change the limit first"
);
lp.add2list(
  "Postpaid na razie jeszcze nie działa. Jeśli chcesz ustawić, aby konto na koniec miesiąca dostawało fakturę to ustaw Stałe wstrzymanie płatności oraz w notatce napisz: faktura vat postpaid na koniec miesiąca",
  "Postpaid doesn’t work yet. If you want the account to receive invoice at the end of the month, then set Permanent suspension of payments and write in note: VAT invoice postpaid at the end of the month"
);
lp.add2list(
  "Na pewno chcesz ustawić na końcie prepaid i zrezygnować z postpaid?",
  "Are you sure you want to switch this account to prepaid and resign from postpaid?"
);
lp.add2list("Czy włączyć na koncie system płatności Veles?", "Turn on Veles Payment System on the account?");
lp.add2list("Czy chcesz anulować płatność?", "Do you wish to cancel payment? ");
lp.add2list("Wystąpił błąd podczas przetwarzania żądania", "Error occurred while processing the task");
lp.add2list("Czy chcesz ponownie aktywować płatność?", "Do you want to reactivate payment?");
lp.add2list(
  "Czy chcesz wygenerować nową płatność (fakturę proforma) przedłużającą ważność konta o kolejny okres abonamentowy?",
  "Do you want to generate a new payment (pro forma invoice) extending the validity of account by another subscription period?"
);
lp.add2list("Problem z ustawieniem rodzaju firmy lub branży", "Problem with setting type of company or industry");
lp.add2list("Wystąpił błąd podczas przetwarzania formularza", "Error occurred while processing the form");
lp.add2list("Pokaż więcej opcji", "Show more options");
lp.add2list("Ukryj więcej opcji", "Hide more options");
lp.add2list("Pokaż predefiniowane filtry i legendę", "Show predefines filters and legend");
lp.add2list("Ukryj predefiniowane filtry i legendę", "Hide predefined filters and legend ");
lp.add2list("Wysłano do sparsowania", "Sent to be parsed");
lp.add2list(
  "Wystąpił błąd. Upewnij się, że uzupełniłeś wszystkie pola i spróbuj ponownie",
  "Error. Please make sure, that you filled all fields and try again"
);
lp.add2list("ukryj", "hide");
lp.add2list("pokaż", "show");
lp.add2list("NIE", "NO");
lp.add2list("TAK", "YES");
lp.add2list("Data utworzenia", "Creation date");
lp.add2list("Treść", "Content");
lp.add2list("Edycja kodu promocyjnego", "Edit a promo code");
lp.add2list("Dodawanie kodu promocyjnego", "Adding a promo code");
lp.add2list("Musisz podać kod", "You have to enter the code");
lp.add2list("Musisz podać status", "You have to enter the status");
lp.add2list("Obliczanie...", "Processing...");
lp.add2list("Zostanie skasowanych:", "Will be deleted:");
lp.add2list("wyników", "Mentions");
lp.add2list("Zobacz te wyniki", "See these mentions");
lp.add2list(
  "Jesteś pewien, że chcesz zablokować tę domenę oraz skasować wszystkie powiązane z nią wyniki?",
  "Are you sure you want to block this domain and delete all mentions  from it?"
);
lp.add2list(
  "Obliczanie... Proszę czekać - może to potrwać kilkadziesiąt sekund",
  "Calculating... Please wait - it may take several dozen minutes"
);
lp.add2list("Zostanie zaktualizowanych ", "Will be updated");
lp.add2list(
  "Jeżeli chcesz edytować kategorie hosta, to uzupełnij najpierw wszystkie pola ;)",
  "If you wish to edit category of host then fill in all fields first ;)"
);
lp.add2list(
  "Jesteś pewien, że chcesz edytowac kategorię tego hosta?",
  "Are you sure you wish to edit category of this host?"
);
lp.add2list(
  "Trwa edytowanie kategorii. Dla dużej ilości wpisów operacja może długo potrwać...",
  "Category edition is in progress. For large number of mentions this operation might take a while..."
);
lp.add2list("Host do sprawdzenia (dodaj do listy)", "Host to check (add to list)");
lp.add2list("Host sprawdzony (usuń z listy)", "Host is checked (remove from list)");
lp.add2list(
  "Czy na pewno chcesz zmienić status tego konta?",
  "Are you sure you want to change status of this account?"
);
lp.add2list("Proszę czekać...", "Please wait...");
lp.add2list("Wyłącz", "Turn off");
lp.add2list("Włącz", "Turn on");
lp.add2list(
  "Proszę czekać, trwa pobieranie nowego access tokena.",
  "Please wait, downloading new access token is in progress."
);
lp.add2list(
  "Nie udało się wygenerować access tokena. <br> Konto zostało zablokowane",
  "Access token has failed to be generated. <br> Account has been blocked"
);
lp.add2list("Wygenerowanie poprawnie acess token: ", "Correct generation of access tokens: ");
lp.add2list("Pole email lub hasło nie może być puste", "Email field or password cannot be empty");
lp.add2list("Czy na pewno chcesz usunąć to konto?", "Are you sure you want to delete this account?");
lp.add2list("Nie udalo sie zapisac notki dla forum o ID: ", "Note has failed to be saved for forum with ID:");
lp.add2list(". Notka: ", ". Note: ");
lp.add2list("(analizuj)", "(analyse)");
lp.add2list("(wyłącz)", "(turn off)");
lp.add2list("(pokaż działy)", "(show sections)");
lp.add2list("(pokaż tematy)", "(show topics)");
lp.add2list("(ost. wpisy)", "(last mentions)");
lp.add2list("Odkryj z powrotem", "Reveal again");
lp.add2list("Ukryj: spam", "Hide: spam");
lp.add2list("Ukryj: inny powód", "Hide: other reason");
lp.add2list("Czy na pewno usunąć kategorię?", "Are you sure you want to delete this category?");
lp.add2list("Czy na pewno usunąć zagadnienie?", "Are you sure you want to delete subject?");
lp.add2list("Zmienić status zagadnienia? ", "Change status of subject?");
lp.add2list(" - blokujesz sam host<br />", " - block only host<br />");
lp.add2list(
  " - blokuje host najwyższy<br />Jak jest jedna opcja: blokujesz host najwyższy!",
  " - blocking highest host<br />If there is only one option: block highest host!"
);
lp.add2list("Zmieniono pomyślnie!", "Successfully changed!");
lp.add2list("Wystąpił nieznany błąd. Spróbuj ponownie", "An unknown error has occurred. Try again");
lp.add2list("Wpis został sparsowany", "Mention has been parsed");
lp.add2list("Czekaj...", "Wait...");
lp.add2list("Nie ma konto o takiej nazwie", "There is no account with this name");
lp.add2list("Twoje konto zostało zablokowane.", "Your account has been blocked.");
lp.add2list("Niepoprawne dane logowania", "Incorrect email or password login.");
lp.add2list("Czy na pewno chcesz uruchomić mailing?", "Are you sure you want to start mailing?");
lp.add2list("Zresetować liczniki kategorii?", "Do you wish to reset counter of categories?");
lp.add2list("Zresetować licznik wybranej kategorii?", "Do you want  to reset counter of category?");
lp.add2list("Wysłano", "Sent");
lp.add2list("Błąd", "Error");
lp.add2list("Blad sciagania HTML: ", "Error of downloading HTML: ");
lp.add2list("Aktywne", "Active");
lp.add2list("Nieaktywne", "Inactive");
lp.add2list("na góre!", "upstairs!");
lp.add2list("Ok. Przestawionych wierszy: ", "Ok. Moved lines:");
lp.add2list("Uzupełnij tytuł lub treść (przynajmniej jedno pole)", "Fill in title or content (at least one field)");
lp.add2list("Uzupełnij adres URL do avataru autora", "Fill in URL address to author's avatar");
lp.add2list("Uzupełnij adres URL autora", "Fill up the author's URL address ");
lp.add2list("Uzupełnij nazwę autora", "Fill up the author's name");
lp.add2list(
  "Uzupełnij liczbę osób śledząych autora (subskrybenci + znajomi)",
  "Fill in the number of people following this author (subscribers + friends)"
);
lp.add2list(
  "Nie masz praw do przeniesienia tego projektu na to konto. Musisz mieć prawa zarówno do starego jak i nowego konta",
  "You have no rights to move project to this account. You need privileges to old and new account"
);
lp.add2list(
  "Możesz ręcznie zmienić częstotliwość tylko w przypadku, gdy Tryb wyboru częstotliwości jest ustawiony na ręczny",
  "You can manually change frequency only when frequency setting mode is set to manual"
);
lp.add2list("Co ile <b>sekund</b> ma być odpalany parser", "How often <b>seconds</b> should parsers be started");
lp.add2list("Nie ma konta o takiej nazwie", "There is no account with this name");
lp.add2list(
  "Nie masz praw do przeniesienia projektu na wybrane konto",
  "You have no rights to move project to this account"
);
lp.add2list("Czy na pewno usunąć?", "Are you sure you want to delete?");
lp.add2list("Czy na pewno chcesz oznaczyć że host ", "Are you sure you want to mark that host ");
lp.add2list("posiada dedykowany parser?", "does it have a dedicated parser?");
lp.add2list("Czy na pewno chcesz usunać oznaczenie dla ", "Are you sure you want to delete the mark for ");
lp.add2list("Host został oznaczony.", "Host has been marked.");
lp.add2list("Oznaczenie zostało usunięte", "Marking has been deleted");
lp.add2list("Możesz wybrać max 10 wartości z listy", "You can choose maximally 10 items from the list");
lp.add2list("Tak, znaleziono", "Yes, it has been found");
lp.add2list("Nie, nie znaleziono", "No, it hasn't been found");
lp.add2list("Tak, usunięty", "Yes, it's deleted");
lp.add2list("Nie, nieusunięty", "No, it's not deleted");
lp.add2list("Na pewno chcesz aktywować użytkownika nr ", "Are you sure you want to activate user nr ");
lp.add2list("Aktywowano", "Activated");
lp.add2list("Użytkownik jest już aktywny", "User is active");
lp.add2list("Zaloguj", "Login");
lp.add2list("Musisz podać hasło", "You have to enter the password");
lp.add2list("Musisz podać email", "You have to enter email address");
lp.add2list("tags.no_manage_privileges", "You have no privileges to administrate tags");
lp.add2list("Nowych wpisów w projektach", "New mentions in projects");
lp.add2list(
  "Twój adres email nie został jeszcze aktywowany, przez co nie możesz korzystać z wszystkich możliwości systemu. Podczas rejestracji wysłaliśmy do Ciebie wiadomość email z linkiem aktywacyjnym, który musisz kliknąć, aby aktywować adres email. Jeśli nie możesz znaleźć tej wiadomości email, wyślemy ją Tobie ponownie",
  "Your email address  has not been activated yet, so you cant use all features.  During the registration we sent you an email with activation link, which you must click to activate email address.  If you can't find email with activation link, we will send it again"
);
lp.add2list("Wyślij ponownie email z linkiem aktywacyjnym", "Send activation link again");
lp.add2list(
  "Link aktywacyjny został wysłany emailem. Sprawdź swoją skrzynkę pocztową. Jeśli nie znajdujesz tam emaila, sprawdź skrzynkę SPAM, niekótre systemy błędnie klasyfikują emaile rejestracyjne",
  "The email with activation link has been sent to your mailbox. If you cannot find it, check the spam folder."
);
lp.add2list("Błąd podczas wysyłania emaila aktywacyjnego", "Account activation email sending error");
lp.add2list("Musisz wyrazić zgodę", "You have to agree");
lp.add2list(
  "W ustawieniach Facebook nie wyraziłeś zgody na przekazanie nam Twojego emaila. Bez tego nie możemy zarejestrować konta. Prosimy o zmianę ustawień na Facebooku lub zarejestrowanie się przy pomocy emaila i hasła.",
  "In the Facebook settings you have not given consent to provide us with your email. Without this we can not register your account. Please change the settings on Facebook or sign up using the email address and password."
);
lp.add2list("Filtr został dodany", "Filter has been added");
lp.add2list("Filtr został zmieniony", "Filter has been changed");
lp.add2list("Wybierz projekt, dla którego chcesz dodać filtr", "Select a project you would like to apply filters to");
lp.add2list("Zwrotów kluczowe", "Keywords");
lp.add2list(
  "Ile będziesz mógł monitorować zwrotów  wybierając dany plan abonamentowy. %s Jeśli potrzebujesz więcej zwrotów możesz wybrać wyższy plan lub wykupić dodatkowy limit (patrz opcja %s Powiększanie limitów %s)",
  "How many keywords will I be able to monitor by choosing a particular service plan? %s If you need to monitor more keywords you can choose another subscription plan or buy extra keywords for your current subscription plan (see %s Extending limits %s)"
);
lp.add2list("Limit wpisów", "Mentions limit");
lp.add2list(
  "Maksymalna liczba wpisów dodawanych przez system w ciągu miesiąca dla całego konta",
  "Maximum number of mentions added by system in one month for the whole account"
);
lp.add2list("Aktualizacja wyników", "Mentions update");
lp.add2list(
  "Aktualizacja wyników określa jak często system będzie przeszukiwać Internet w poszukiwaniu zadanych zwrotów, czyli jak często będą pojawiać się nowe wyniki",
  "This describes how often the system crawls the web in search of chosen keywords. This also determines how often new mentions will be shown in the dashboard"
);
lp.add2list("Archiwum wyników", "Mentions archive");
lp.add2list(
  "Jak długo będą przechowywane znalezione wypowiedzi. Opcja <i>Pełne archiwum</i> nie wprowadza ograniczeń na czas przechowywania.",
  "How long the gathered mentions will be kept. Optional: <i>unlimited storage of full archive</i> for your mentions"
);
lp.add2list("Liczba użytkowników", "Number of users");
lp.add2list(
  "Ilu użytkowników będzie mogło korzystać z Twojego konta. Przyznając dostęp kolejnym użytkownikom będziesz mógł nadać im pełne prawa do konta lub tylko do wybranych projektów.",
  "How many users can use your account? By giving account access to other users, you can determine their rights within your account. %s If you need to give access to more users, you can choose a higher service plan or buy extra logins (see %s Extending limits%s)"
);
lp.add2list(
  "Ilu użytkowników będzie mogło korzystać z Twojego konta. Przyznając dostęp kolejnym użytkownikom będziesz mógł nadać im pełne prawa do konta lub tylko do wybranych projektów. %s Jeśli potrzebujesz przyznać dostęp większej ilości użytkowników możesz wybrać wyższy plan lub wykupić dodatkowy limit (patrz opcja %s Powiększanie limitów%s)",
  "How many users can use your account? By giving account access to other users, you can determine their rights within your account. %s If you need to give access to more users, you can choose a higher service plan or buy extra logins (see %s Extending limits%s)"
);
lp.add2list("Analiza danych", "Data analysis");
lp.add2list(
  "Dostęp do narzędzi pozwalających analizować zebrane wypowiedzi z Internetu",
  "Access to tools which let you analyze the data you’ve gathered from the Internet"
);
lp.add2list("Analiza sentymentu", "Sentiment Analysis");
lp.add2list(
  "Automatyczne określanie przez specjalny algorytm sentymentu znalezionej wypowiedzi (pozytywna, neutralna, negatywna)",
  "This algorithm automatically detects the sentiment of your gathered data (positive, neutral, negative)"
);
lp.add2list("Pełne raporty", "Full raports");
lp.add2list("Dostęp do raportów za wybrane okresy czasu", "Access to the reports in desired time span");
lp.add2list("Pomoc live", "Live help");
lp.add2list(
  "Dostęp do pomocy live 24/7/365 - poprzez chat na stronie, email lub telefonicznie",
  "Access to live help 24/7/365 via chat, email or phone"
);
lp.add2list(
  "Szacowana ilość osób, które mogły mieć styczność z wypowiedziami w Social Media zawierającymi monitorowane zwroty. Zasięg szacowany jest na podstawie ilości autorów wypowiadających się w Social Media na temat marki, liczby ich znajomych lub śledzących oraz współczynnika widzialności treści typowych dla wybranych serwisów społecznościowych",
  "Estimated number of people on social media who could have seen or interacted with the relevant mentions (containing the monitored keyword(s)).<br /><br >Estimated social media reach is based on:<br />(1) the number of authors who are talking about your monitored phrases on social media,<br />(2) the number of their followers/subscribers/friends, and<br />(3) the Visibilty Percentage* for the selected social network.<br /><br />* This percentage describes how many of your friends/followers actually see your posts on average."
);
lp.add2list("tags.title", "Tags");
lp.add2list(
  "tags.pricelist_info",
  "Ability to create tags of mentions which makes analyzing the data easier. Very useful feature for collaborating with team"
);
lp.add2list("Liderzy opinii", "Opinion leaders");
lp.add2list(
  "Dostęp do pełnego zestawienia osób najczęściej wypowiadających się na temat Twojej marki",
  "Access to the full collation of people who comment on your brand the most"
);
lp.add2list("Export danych", "Data export");
lp.add2list(
  "Możliwość eksportu znalezionych wypowiedzi w postaci pliku Excel",
  "Ability to export all gathered mentions to Excel"
);
lp.add2list("Raporty PDF", "PDF reports");
lp.add2list(
  "Raporty PDF umożliwiają automatyczne tworzenie  analiz zestawiających podstawowe dane na temat monitorowanej marki, produktu czy tematu do pliku PDF. Wystarczy jeden klik aby po wyborze okresu monitoringu wygenerowany został przejrzysty raport opatrzony Twoim logo i wybraną kolorystyką",
  "Create automatic analyses from the gathered data about the brand, product, etc. It only takes one click to generate a clear report for your selected time period"
);
lp.add2list("Konsultant", "Consultant ");
lp.add2list(
  "Dedykowana osoba po stronie %s pomagająca konfigurować projekty, dobierać najlepszy zestaw słów kluczowych oraz tworzyć analizy",
  "A dedicated member of %s will help you configure projects, select the best keywords, and create analyses"
);
lp.add2list(
  "Możliwość dostępu do znalezionych wyników poprzez interfejs XML",
  "Ability to access the gathered data via XML interface"
);
lp.add2list("Powiększenie limitów", "Limits extension");
lp.add2list("Interakcje", "Interactions");
lp.add2list(
  "Informacje o popularności znalezionych wpisów w wybranych serwisach social media",
  "Information about popularity of found mentions in specified social media platforms"
);
lp.add2list(
  "Z jakiego okresu ładowane są wyniki podczas tworzenia projektu",
  "From which period your mentions are uploaded to a project"
);
lp.add2list(
  "Wskazanie najpopularniejszych słów w dyskusjach dotyczących monitorowanej marki",
  "The most popular words in mentions from recent days which include monitored phrases"
);
lp.add2list(
  "Dziękujemy za zgłoszenie. Skontaktujemy się z Tobą.",
  "Thank you for your notification. We will contact you"
);
lp.add2list("Podaj poprawny email", "Type in the correct email address");
lp.add2list("Email został już wcześniej zgłoszony", "The email address is already in the database");
lp.add2list("Błąd formularza", "Form error");
lp.add2list(
  "Aktualnie jesteś zalogowany na koncie %s%s%s. Chcesz zmienić jego plan abonamentowy, czy zarejestrować nowe konto?",
  "Currently you are logged in to account %s%s%s. Do you want to change the subscription plan or register a new account?"
);
lp.add2list("Chcę zmienić plan", "I want to change the subscription plan");
lp.add2list("Chcę zarejstrować nowe konto", "I want to register new account");
lp.add2list("Ni", "Sun");
lp.add2list("Po", "Mon");
lp.add2list("Wt", "Tue");
lp.add2list("Śr", "wed");
lp.add2list("Cz", "Th");
lp.add2list("Pi", "Fri");
lp.add2list("So", "Sat");
lp.add2list("Styczeń", "Jan");
lp.add2list("Luty", "Feb");
lp.add2list("Marzec", "Mar");
lp.add2list("Kwiecień", "Apr");
lp.add2list("Maj", "May");
lp.add2list("Czerwiec", "Jun");
lp.add2list("Lipiec", "Jul");
lp.add2list("Sierpień", "Aug");
lp.add2list("Wrzesień", "Sept");
lp.add2list("Październik", "Oct");
lp.add2list("Listopad", "Nov");
lp.add2list("Grudzień", "Dec");
lp.add2list("zaznacz", "select");
lp.add2list(" - zdjęcie w serwisie Instagram", " - picture on Instagram");
lp.add2list("Zdjęcie", "Picture");
lp.add2list("Brak treści do wyświetlenia", "No content to display");
lp.add2list("Źródło", "Source");
lp.add2list("W projekcie istnieje wpis o takim adresie", "There is entry with this address in the project");
lp.add2list("Adres wpisu ma nieprawidłową budowę", "Address of the entry has incorrect form");
lp.add2list("Uzupełnij url wpisu", "Complete the entry url");
lp.add2list("Uzupełnij treść wpisu", "Complete the entry content");
lp.add2list("Data wpisu nie może być pusta", "Date of the entry cannot be empty");
lp.add2list("Godzina wpisu jest niepoprawna (0-23)", "Hour of the entry is not correct (0-23)");
lp.add2list("Minuta wpisu jest niepoprawna (0-59)", "Minute of the entry is not correct (0-59)");
lp.add2list("Wystąpił błąd. Spróbuj jeszcze raz.", "An error occurred. Try once again.");
lp.add2list(
  "Dodawanie fanpage jest chwilowo niedostępne. Proszę spróbować za chwilę.",
  "Adding fanpage is temporary unavailable. Please try again in a moment."
);
lp.add2list("brak wyników", "No mentions");
lp.add2list("osób lubi to", "people like it");
lp.add2list("Strona", "Page");
lp.add2list("Opis", "Description");
lp.add2list(
  "Czy na pewno chcesz opuścić formularz edycji projektu bez zapisania?",
  "Are you sure you want to quit the edit form of the project without saving?"
);
lp.add2list("Nie, pozostaw stare wpisy", "No, keep existing mentions");
lp.add2list("Tak, usuń wszystkie wpisy i ściągnij nowe", "Yes, delete existing mentions and collect new ones");
lp.add2list("Usuwanie starych wpisów i ściągnie nowych....", "Deleting existing mentions and collecting new ones....");
lp.add2list(
  "Uwaga! Zmiany w konfiguracji projektu spowodują, że dotychczasowo zebrane wpisy nie będą pasować do ustawień monitorowanych słów.<br /><br />Czy chcesz pozostawić dotychczas zebrane wyniki nawet jeżeli nie spełniają nowej konfiguracji, czy usunąć je i zebrać nowe wpisy pasujące do nowej konfiguracji?",
  "Please note! You've made some changes, and the mentions you already collected no longer apply to keywords settings.<br /><br />Would you like to keep these mentions, or delete them and collect new ones which apply to the new configurations for your project?"
);
lp.add2list("Podaj słowo kluczowe", "Enter the keyword");
lp.add2list("Ups, Twój projekt nadal potrzebuje słów kluczowych!", "Oops, your project still needs some keywords!");
lp.add2list(
  "Bedziemy poszukiwac wpisów, w których wystąpi zwrot pino, ale nie beda zawierały slowa portugalia",
  "We will be looking for mentions with phrase pino, but they will not contain word Portugal"
);
lp.add2list("Dodawanie powiadomienia", "Adding the new notification");
lp.add2list("Ten użytkownik już raz był użyty", "The user you selected has already signed up for this report");
lp.add2list("dodaj nowego użytkownika", "add new recipient");
lp.add2list(
  "Wybrana opcja jest w trakcie budowy. Obecnie, aby zablokować żródło, możesz to zrobić klikając USUŃ przy znalezionym wyniku",
  'Selected feature is under construction. Currently, to block the source, you need to click "delete" next to found mention'
);
lp.add2list("Podaj słowa kluczowe oddzielone przecinkiem", "separate words with comma");
lp.add2list("Brak podpowiedzi", "Avg.visits");
lp.add2list("Możesz wybrać max 2 kategorie", "You can choose max 2 categories ");
lp.add2list(
  "Podaj co najmniej jedno konto - Twitter, Facebook, Instagram",
  "Type in at least one account - Twitter, Facebook, Instagram"
);
lp.add2list("Wybierz co najmniej 1 państwo", "Choose at least 1 country");
lp.add2list("Wybierz co najmniej 1 kategorię", "Choose at least one category");
lp.add2list(
  "Dziękujemy za dodanie osoby. Wkrótce moderator umieści ją na stronie!",
  "Thank you for adding this person. It will appear on the page shortly. "
);
lp.add2list("Możesz wybrać max 2 wartości z listy", "You can choose maximally 2 items from the list");
lp.add2list("Taki email jest już w bazie", "This email address is already in database");
lp.add2list("Własny", "Own");
lp.add2list("Dzisiaj", "Today");
lp.add2list("Wczoraj", "Yesterday");
lp.add2list("Ostatnie 7 dni", "Last 7 days");
lp.add2list("Ostatnie 30 dni", "Last 30 days");
lp.add2list("Ostatnie 3 miesiące", "Last 3 months");
lp.add2list("Ostatni rok", "Last year");
lp.add2list("Wszystko", "All");
lp.add2list("Okres", "Period");
lp.add2list("Ustaw okres jako domyślny", "Set as default period");
lp.add2list("Zastosuj", "Apply");
lp.add2list("Dni", "Days");
lp.add2list("Tygodnie", "Weeks");
lp.add2list("Miesiące", "Months");
lp.add2list("Wybierz daty i okres", "Pick a date range and period");
lp.add2list("Obecnie wybrany projekt", "Your current project");
lp.add2list("Najnowsze Wyniki", "Latest Mentions");
lp.add2list("Najbardziej wpływowe profile", "The most influential profiles");
lp.add2list("Najbardziej wpływowe strony", "The most influential sites");
lp.add2list("Zmień plan", "Upgrade");
lp.add2list("Najpopularniejsze wzmianki", "The most popular mentions");
lp.add2list("Niedostępne w Twoim planie.", "Not available for your plan.");
lp.add2list("Najnowsze wzmianki", "Latest mentions");
lp.add2list("Więcej wyników", "More mentions");
lp.add2list("Wyników poza social media", "Mentions beyond social media ");
lp.add2list("Wyników w social media", "Social media Mentions");
lp.add2list("Komentarzy w social media", "Social media comments");
lp.add2list("Udostępnień w social media", "Social media shares");
lp.add2list("Polubień w social media", "Social media likes");
lp.add2list("Statystyki", "Stats");
lp.add2list("Obecny okres", "Current period");
lp.add2list("Poprzedni okres", "Previous period");
lp.add2list("Podsumowanie", "Summary");
lp.add2list("Pojawiły się nowe wzmianki w Twoim projekcie.", "We collected additional data for your project.");
lp.add2list("Kliknij, aby zobaczyć.", "Refresh to access.");
lp.add2list("Mld", "B");
lp.add2list("remove_project", "Remove project");
lp.add2list("save", "Save");
lp.add2list("save.as_new", "Save as new");
lp.add2list("filters.geolocation.alphabetical_order", "Alphabetical order");
lp.add2list("filters.geolocation.most_popular_order", "Most popular");
lp.add2list(
  "filters.text_search_input.qtip",
  "Here you can browse your mentions more thoroughly. To look even deeper within your project, you can use the following operators: <i>{AND}, {OR}, {NOT}</i>. Example: <i>Tablet {AND} {NOT} laptop</i> – in this case, Brand24 finds mentions containing the word <i>tablet</i> but not <i>laptop</i></br> Using <i>{IS}</i>, search results can be narrowed down to mentions containing an exactly specified word, for example, with <i>{IS}red</i> the tool will find all mentions containing <i>red</i> but not <i>redbull</i>."
);
lp.add2list("hashtags_analytics.trending.table_error", "Error occurred while loading hashtags");
lp.add2list("hashtags_analytics.trending.tabe_try_again", "Try again");
lp.add2list("cancel_subscription.confirmation_modal.close", "Bring me back");
lp.add2list("cancel_subscription.confirmation_modal.agree", "Cancel subscription");
lp.add2list("cancel_subscription.confirmation_modal.success.title", "Subscription cancelled");
lp.add2list("cancel_subscription.confirmation_modal.success.primary_action", "Got it!");
lp.add2list("cancel_subscription.confirmation_modal.error.title", "Whoops...");
lp.add2list("cancel_subscription.confirmation_modal.error.primary_action", "OK");
lp.add2list("facebook_fanpage.removal_modal.title", "Remove fanpage");
lp.add2list("facebook_fanpage.removal_modal.close", "Bring me back");
lp.add2list("facebook_fanpage.removal_modal.remove", "Remove fanpage");
lp.add2list(
  "account.pricing.extending_limits",
  "In case you need it, you can extend the limits of your account at any time. <br /><br />Prices from the chosen subscription plan:<br /><br />Extra keyword: 15 USD monthly <br />Extra user: 15 USD monthly"
);
lp.add2list("change_billing_cycle.confirmation_modal.success.primary_action", "Got it!");
lp.add2list("change_billing_cycle.confirmation_modal.error.primary_action", "Ok");
lp.add2list("change_billing_cycle.confirmation_modal.close", "Cancel process");
lp.add2list("change_billing_cycle.confirmation_modal.agree", "Pay now");
lp.add2list("change_billing_cycle.not_available_modal.primary_action", "Ok");
lp.add2list("mention-order.select.recent", "Recent first");
lp.add2list("mention-order.select.popular", "Popular first");
lp.add2list("mention-order.select.important", "Important first");
lp.add2list("cms.early_access.grant_error", "There was an error when grating access");
lp.add2list("mention.options.pdfGroupShortcut.add", "Add to PDF Report");
lp.add2list("mention.options.pdfGroupShortcut.remove", "Added to PDF Report");
lp.add2list("twitter_policy_modal.close", "Close");
lp.add2list(
  "twitter_policy_modal.newMessageTemplate",
  "Hello, I would like to verify my account to enable Twitter Data."
);
lp.add2list("twitter_policy_modal.cta", "I accept above rules. Turn on collecting Twitter mentions");
lp.add2list("twitter_policy_modal.contact_us", "Contact us to verify and activate collecting Twitter mentions");
lp.add2list("twitter_policy_modal.twitter_data_is_being_unblocked", "Twitter data is being unblocked...");
lp.add2list("twitter_policy_modal.twitter_data_enabled_on_your_account", "Twitter data enabled on your account");
lp.add2list("no_twitter_congratulations_modal.cta", "Ok");
lp.add2list("twitter_congratulations_modal.cta", "Ok!");
lp.add2list("twitter_congratulations_modal.contact_us", "Contact us");
lp.add2list(
  "project_settings.add_mention.not_supported_url",
  "Sorry, we don't support adding mentions from Facebook or Instagram at this time."
);
lp.add2list(
  "trial_top_notification.upgrade.message",
  "Trial Version with limited data. Upgrade to gain access to all collected public mentions."
);
lp.add2list(
  "trial_notification.upgrade.message",
  "Trial Version with limited data. Upgrade to gain access to all collected public mentions."
);
lp.add2list("trial_notification.upgrade.cta", "Upgrade");
lp.add2list("noResultsFilters.title", "We couldn't find mentions for selected filters");
lp.add2list("noResultsFilters.subtitle", "To display all collected mentions please try clearing selected filters");
lp.add2list("noResultsFilters.cta", "Reset filters");
lp.add2list("noResults.title", "We couldn't find mentions for your keywords");
lp.add2list("noResults.subtitle", "There might be few reasons why we couldn't find mentions for entered keywords");
lp.add2list("noResults.cta", "Create new project");
lp.add2list("noResults.languageCta", "Remove language filter");
lp.add2list("noResults.reason1", "Check if inserted keywords are written correctly without mistake");
lp.add2list("noResults.reason2", 'Try using different "Language" choice or try "All languages" option');
lp.add2list("noResults.reason_instagram", "Keywords you used are exclusively used on Instagram.");
lp.add2list("noResults.languageReason", "No mentions found for selected language filter");
lp.add2list("noResults.integrate_facebook", "Integrate Facebook / Instagram");
lp.add2list("noResults.or", "OR");
lp.add2list("panel.new_mention_add.limit_exceeded", "New mentions limit exceeded");
lp.add2list("panel.importance.sentiment_importance.1", "Not emotional");
lp.add2list("panel.importance.sentiment_importance.2", "Moderately emotional");
lp.add2list("panel.importance.sentiment_importance.3", "Highly emotional");
lp.add2list("panel.importance.host_importance.1", "Low traffic domain");
lp.add2list("panel.importance.host_importance.2", "Medium traffic domain");
lp.add2list("panel.importance.host_importance.3", "High traffic domain");
lp.add2list("panel.importance.author_importance.1", "Low influential author");
lp.add2list("panel.importance.author_importance.2", "Medium influential author");
lp.add2list("panel.importance.author_importance.3", "Highly influential author");
lp.add2list("panel.importance.interactions_importance.1", "Low interactions number");
lp.add2list("panel.importance.interactions_importance.2", "Medium interactions number");
lp.add2list("panel.importance.interactions_importance.3", "High interactions number");
lp.add2list("panel.importance.category_importance.3", "Valuable category");
lp.add2list("panel.importance.news_importance.3", "Valuable news");
lp.add2list("Grupuj", "Tag");
lp.add2list("user.register.google_error", "Can't sign in with Google.");
lp.add2list("cms.account_type.contacts_history_adding_new_contact", "Adding new contact");
lp.add2list("cms.account_type.contacts_history_edit_contact", "Edit contact");
lp.add2list("cms.account_type.contacts_history_options_delete_are_you_sure", "Are you sure?");
lp.add2list("cms.account_type.contacts_history_options_delete_are_you_sure_yes", "Yes");
lp.add2list("cms.account_type.contacts_history_options_delete_are_you_sure_no", "No");
lp.add2list("cms.account_type.contacts_history_options_delete_deleted", "Deleted");
lp.add2list("cms.account_type.contacts_history_options_delete_error", "Error");
lp.add2list("cms.account_type.contacts_history_edit_contact_submit_error", "Error");
lp.add2list("project.results.non_social_reach", "Non social reach");
lp.add2list("project.results.instagram_reach", "Instagram reach");
lp.add2list("mentions.chart.mentions_count", "Number of mentions");
lp.add2list("mentions.chart.social_media_reach", "Social Media Reach");
lp.add2list("mentions.chart.non_social_reach", "Non Social Reach");
lp.add2list(
  "newsletter_mention.help_tooltip",
  "Newsletter Mentions are un-clickable (not all of them have web version)."
);
lp.add2list("panel.notify_incorrect_sentiment", "Notify incorrect sentiment");
lp.add2list("pricing.feature.results_updating", "Mentions update");
lp.add2list(
  "pricing.feature.results_updating.text_max",
  "Real-Time means updates for the major sources of mentions every 15 minutes. The major sources consist of popular social platforms, as well as some of the non-social sources."
);
lp.add2list("payment.credit_card.no_card_holder_name_error", "No card holder name");
lp.add2list("payment.credit_card.no_card_number_error", "No card number");
lp.add2list("payment.credit_card.no_card_expiry_error", "No card expiration date");
lp.add2list("payment.credit_card.no_card_cvc_error", "No card CVC number");
lp.add2list(
  "pricing.feature.fb_and_instagram.description",
  "Integrate with Facebook to track brand mentions on Facebook and Instagram."
);
lp.add2list(
  "pricing.feature.social_media_monitoring.description",
  "Track brand mentions across popular Social Media platforms."
);
lp.add2list(
  "pricing.feature.newsletter_monitoring.description",
  "Track brand mentions across popular, public newsletters all over the World. Brand24 currently collects over 40,000 daily mentions from public newsletters. You can also add new public newsletters to track within your Brand24 account."
);
lp.add2list(
  "pricing.feature.podcast_monitoring.description",
  "Track brand mentions across podcasts. Brand24 tracks 800,000 active podcasts.\n"
);
lp.add2list("panel.importance.reputation_disclaimer.excellent", "Excellent");
lp.add2list("panel.importance.reputation_disclaimer.good", "Good");
lp.add2list("panel.importance.reputation_disclaimer.average", "Average");
lp.add2list("panel.importance.reputation_disclaimer.bad", "Bad");
lp.add2list("panel.importance.reputation_comparison_0", "With a score of ");
lp.add2list("panel.importance.reputation_comparison_1", " your Reputation is better than ");
lp.add2list("panel.importance.awareness_comparison_1", " your Presence Score is better than ");
lp.add2list("panel.importance.reputation_comparison_2", " of brands.");
lp.add2list("panel.importance.reputation", "Reputation");
lp.add2list("panel.importance.reputation.change_description", "Changed by");
lp.add2list("panel.importance.reputation.change_reason_description", "because of");
lp.add2list("panel.importance.reputation.no_change", "No change in score");
lp.add2list("panel.importance.reputation.reason_join", "due to");
lp.add2list("panel.importance.reputation.change_reason_0", "insufficient number of emotional mentions");
lp.add2list("panel.importance.reputation.change_reason_1", "dominant number of positive mentions");
lp.add2list("panel.importance.reputation.change_reason_2", "dominant number of negative mentions");
lp.add2list("panel.importance.reputation.change_reason_3", "balanced number of positive and negative mentions");
lp.add2list("panel.importance.reputation.change_reason_4", "spike in number of negative mentions");
lp.add2list("panel.importance.reputation.change_reason_5", "recent reputation decrease");
lp.add2list("panel.importance.reputation.change_reason_6", "insufficient number of mentions");
lp.add2list("panel.importance.reputation.emotional", "Emotional mentions");
lp.add2list("panel.importance.reputation.positives", "positives");
lp.add2list("panel.importance.reputation.negatives", "negatives");
lp.add2list("panel.importance.reputation_chart_loading", "Loading Reputation chart");
lp.add2list("panel.importance.awareness_chart_loading", "Loading Presence chart");
lp.add2list("panel.importance.awareness", "Presence");
lp.add2list("analysis.stats.awareness.chart.legend", "Presence Score");
lp.add2list("analysis.stats.reputation.chart.legend", "Reputation Score");
lp.add2list("analysis.stats.reputation.chart.legend_positives", "Positive mentions number");
lp.add2list("analysis.stats.reputation.chart.legend_negatives", "Negative mentions number");
lp.add2list("analysis.stats.awareness.chart.tooltip.awareness_value", "Presence Score");
lp.add2list("analysis.stats.reputation.chart.tooltip.reputation_value", "Reputation Score");
lp.add2list("analysis.popular_links.header", "Trending links");
lp.add2list("analysis.popular_links.description_less", "Under 10 mentions");
lp.add2list("analysis.popular_links.description_more", "%s+ mentions");
lp.add2list(
  "analysis.popular_links.tooltip",
  "List of most frequently shared links in mentions collected for your search keywords"
);
lp.add2list(
  "pricing.feature.history.description",
  "Period from which the results are loaded when creating the project. For technical reasons, we cannot guarantee full data coverage."
);
lp.add2list(
  "pricing.feature.interactions.description",
  "Information on the popularity of the entries found in selected social media websites. Interactions are updated 3x from the moment the mention appears in the Brand24 panel. The last update takes place 24 hours after the mention appears."
);
lp.add2list("pricing.feature.live_help.description", "Access to live support via chat, email or telephone.");
lp.add2list("analysis.stats.reputation.benchmark.content", "is higher than %s% of brands");
lp.add2list("analysis.stats.awareness.benchmark.content", "is higher than %s% of brands");
lp.add2list(
  "panel.excel_download.account_upgrade",
  "Your current account subscription plan does not allow to use this option."
);
lp.add2list("panel.excel_download.account_upgrade_cta", "Upgrade plan to download the report");
lp.add2list(
  "panel.analysis.context-of-discussion-loading",
  "It may take several minutes to crunch all the data needed for context of discussion analysis"
);
lp.add2list(
  "panel.analysis.context-of-discussion-no-data",
  "Unfortunately, there is not enough data in the project to create context of discussion analysis"
);
lp.add2list(
  "panel.analysis.context-of-discussion-error",
  "Error occurred while loading context of discussion analysis"
);
lp.add2list("panel.analysis.context-of-discussion-error-try-again", "Try again");

