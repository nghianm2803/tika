function LanguageProvider() {
  this.texts_orig = new Array(); //original
  this.texts_tran = new Array(); //translated

  this._ = function (text, params) {
    var output = null;
    idx = $.inArray(text, this.texts_orig);
    if (idx != -1)
      //exists
      output = this.texts_tran[idx];
    else output = text;
    if (params == undefined || params.length == 0)
      //no params
      return output;
    else return this.sprintf(output, params);
  };

  /** Method for adding text with translations into translation list
   * this step is performed by generated script
   */
  this.add2list = function (orig, tran) {
    this.texts_orig.push(orig);
    this.texts_tran.push(tran);
  };

  /** Method for formating text with parameters
   * @param string - string to be formated
   * @param params - array of parameters, can be empty, e.g. ['One', 'Two', 'Three']
   */
  this.sprintf = function (string, params) {
    for (var i = 0; i < params.length; i++) string = string.replace(/%s/, params[i]);
    return string;
  };
}

var lp = new LanguageProvider();
